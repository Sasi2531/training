/*
Requirement:
    Can we write only try block without catch and finally blocks? why?
*/        
Explanation:
    No, It shown  as compilation error.
    The try block must be followed by either catch or finally block. 
    You can remove either catch block or finally block but not both.