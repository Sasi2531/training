/*
Requirement:Handle and give the reason for the exception in the following code: 
             PROGRAM:
             public class Exception {  
             	public static void main(String[] args) {
  	                int arr[] ={1,2,3,4,5};
	                System.out.println(arr[7]);
                }
             }
          Display the output.
          
Entity:
   DisplayOutput
   
Function Declaration:
   public static void main(String[] args)
   
Jobs To Be Done:Consider the below program
                Inside the main method an Array is created name array
                Then the array elements are iterated using for loop
                Inside the for loop try block is given with multiple catch block
                If the exception is raised in try block it will handled using catch block    
 */


package com.java.training.core.exceptionHandling;

public class DisplayOutput {

	public static void main(String[] args) {
		int arr[] ={1,2,3,4,5};
    	
    	try {
            System.out.println(arr[7]);
    	} catch(ArrayIndexOutOfBoundsException e) {
			System.out.println("Array index out of range");
    	}

	}

}
