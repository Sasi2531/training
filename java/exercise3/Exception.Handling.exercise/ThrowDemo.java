/*9)DIFFERENCE BETWEEN THROWS AND THROW:
  1)Syntax wise:
        *Throw is followed by an instance of Exception class.
-------Example---------
        throw new ArithmeticException("Arithmetic Exception");//user-defined
          
        *Throws is followed by exception class names.
-------Example---------
          throws ArithmeticException;
  2)Throw keyword is used in the method body to throw an exception,while throws is used in method signature to declare the exceptions.
-------Example---------:Throw
                        void myMethod() {
try {
      //throwing arithmetic exception using throw
      throw new ArithmeticException("Something went wrong!!");
} 
catch (Exception exp) {
      System.out.println("Error");
      }
}
---------Example--------:Throws
                         void sample() throws ArithmeticException{
                          }

  3)Can throw one exception at a time but can handle multiple exceptions by declaring them using throws keyword.

 Requirements:
   -throw with example. 
 Entity:
   - ExampleThrow
 Function Declaration:
   - public static void main(String[] args)
 Jobs to be done:
    1.If the value given is equal to the value passed in if condition ,it throw an exception.//user-defined.
    2.Otherwise,else part will be executed.
    3.Display the output.
*/



package com.java.training.core.exceptionHandling;

public class ThrowDemo {
	void checkAge(int age){  
		if(age<18)  
		   throw new ArithmeticException("Not Eligible for voting");  
		else  
		   System.out.println("Eligible for voting");  
	   }  

	public static void main(String[] args) {
		 ThrowDemo obj = new ThrowDemo();
			obj.checkAge(13);  
			System.out.println("End Of Program");  
		    

	}

}
