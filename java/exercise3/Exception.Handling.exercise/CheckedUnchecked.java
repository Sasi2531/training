/*
Requirement:
   To Compare the checked and unchecked exception.
   
Entity:
   CheckedUnchecked
   
Function Declaration:
   public static void main(String[] args)
                      static void error()
Jobs To Be Done:
                Consider the Program
                In the main method an exception is raised in the try blocked
                It is handled using catch block during the compile time
                Two variables  a and b of type integer is created 
                In the print statement a/b operation is performed
                But it will raise an error because b stores the value 0
                It will raise exception only in the runtime and not in compile time
 */

package com.java.training.core.exceptionHandling;

public class CheckedUnchecked {
	
	static void error() throws Exception {
		throw new Exception();
	}
	
	public static void main(String[] args) {
		
		try {
		  
		   //unchecked exception it must be handled otherwise rises exception in compile time
		   error();
		} catch(Exception e) {
			System.out.println("No such method  found"); 
		}
		int a = 5;
		int b = 0;
	    System.out.println(a/b);//shows error during runtime not in compile time
	}


}
