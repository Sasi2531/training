/*
Requirement:
    catching multiple exception with example. 
    
 Entities:
    MultipleCatch
    
 Function Declaration:
    public static void main(String[] args)
    
 Jobs to be Done:Consider the below program
                 Inside the main method an Array is created name array
                 Then the array elements are iterated using for loop
                 Inside the for loop try block is given with multiple catch block
                 If the exception is raised in try block it will handled using catch block
 */


package com.java.training.core.exceptionHandling;
import java.util.*;

public class MultipleCatch {

	public static void main(String[] args) {
		int[] array = new int[] {1,2,3,4,5};
		for(int i=0; i<5; i++) {
			
		    try {
			    System.out.println(2/array[i]);
			}
		    catch(ArrayIndexOutOfBoundsException e) {
				System.out.println("Array index out of range");
		    }
		    catch(Exception e) {
				System.out.println("Exception is handled");
		    }
		} 
	}


}
