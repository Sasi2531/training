/*
Requirement:
   Difference between catching multiple exceptions and Mutiple catch blocks.
   
Entity:
   DifferenceMultipleCatch
   
Function Declaration:
   public static void main(String[] args)
   
Jobs To Be Done:Consider this program
                Inside the main method an Array is created name array
                Then the array elements are iterated using for loop
                Inside the for loop try block is given with multiple catch block
                If the exception is raised in try block it will handled using catch block                

Difference:
     -In Multiple catch blocks, try block will throw one exception and have several catch block to 
      handle unknown exception that is thrown. 
     -In multiple exception,  single try block will throw several exception which has to be caught.
*/


package com.java.training.core.exceptionHandling;

public class DifferenceMultipleCatch {

	public static void main(String[] args) {
		
		 int[] array = new int[] {1,2,3,0,5};
	        
			for(int i=0; i<6; i++) {
				
			    try {
				    System.out.println(2/array[i]);
				}
			    catch(ArrayIndexOutOfBoundsException e) {
					System.out.println("Array size out of range");
			    }
			    
			    catch(ArithmeticException e) {
					System.out.println("divided by 0 exception");
			    }
			    
			    catch(Exception e) {
					System.out.println("Exception handled");
			    }
			} 

	}

}
