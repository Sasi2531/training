/*
Requirement:
   demonstrate linked hash set to array() method in java
 
Entity:
   ToArrayDemo
   
Function Declaration:
   public static void mian(String[] args){ }
   
Jobs To Be Done:
   1)Create a linkedhashset
   2)Add elements o the linkedhashset
   3)create a string array
   4)Convert the linkedhashset to array and store in array
   5)iterte the array
      5.1)print the element in the array

pseudo code:
   public class ToArrayDemo {
        
        public static void main(String[] args) {
           
            LinkedHashSet<String> linkedHashSet = new LinkedHashSet<String>();
            add elements to the linkedhashset
            String[] array = new String[];
            array = linkedHashSet.toarray(array);
            for(int number = 0 ;number < arr.length ;number++) {
			       System.out.println(array[i]);
	      	}
        }       
    }
 
*/


package com.java.training.core.set;
import java.util.LinkedHashSet;

public class ToArrayDemo {

	public static void main(String[] args) {
LinkedHashSet<String> linkedHashSet = new LinkedHashSet<String>();
		
		linkedHashSet.add("apple");
		linkedHashSet.add("banana");
		linkedHashSet.add("carrot");
		linkedHashSet.add("dolphin");
		
		System.out.println(linkedHashSet);
		
		String[] array = new String[4];
		array = linkedHashSet.toArray(array);
		
		for(int number = 0 ;number < array.length ;number++) {
			System.out.println(array[number] );
		}
		
	}

}
