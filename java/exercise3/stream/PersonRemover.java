/*
 * Problem Statement
 * 1.Remove the only person who are in the newRoster from the roster list.
 * 		1.1Remove the following person from the roster List
 * 
 * Requirement
 * 1.Remove the only person who are in the newRoster from the roster list.
 * 		1.1Remove the following person from the roster List
 * 
 * Method Signature
 * 
 * Entity
 * 1.PersonRemover
 * 
 * Jobs to be Done
 * 1.Invoke the Person Class createRoster method and store it in persons list.
 * 2.Use removeAll method to remove the newRoster items from the persons items
 * 3.Filter the persons list 
 * 		3.1)If any person is present with the name Bob
 * 		3.2)If there is anyone then remove the person from the persons list
 * 		3.3)Else do nothing
 * 4.Check the persons list whether there is anyone with name Bob or not
 * 		4.1)If there is anyone with name Bob then print true
 * 		4.2)If no then print false
 * 
 * Pseudo Code
 * List<Person> persons = Person.createRoster();
 * persons.removeAll(newRoster);
 *			
 * persons.stream().filter(person -> person.name == "Bob" ? persons.remove(person) : null);
 *		
 * if(persons.contains("Bob")) {
 *		System.out.println("True");
 * } else {
 *		System.out.println("False");
 * }
 *
 *
 */



package com.java.training.core.stream;
import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.List;


public class PersonRemover {
	public static void main(String[] args) {
		List<Person> persons = Person.createRoster();
		
		List<Person> newRoster = new ArrayList<>();
        newRoster.add(
            new Person(
            "John",
            IsoChronology.INSTANCE.date(1980, 6, 20),
            Person.Sex.MALE,
            "john@example.com"));
        newRoster.add(
            new Person(
            "Jade",
            IsoChronology.INSTANCE.date(1990, 7, 15),
            Person.Sex.FEMALE, "jade@example.com"));
        newRoster.add(
            new Person(
            "Donald",
            IsoChronology.INSTANCE.date(1991, 8, 13),
            Person.Sex.MALE, "donald@example.com"));
        newRoster.add(
            new Person(
            "Bob",
            IsoChronology.INSTANCE.date(2000, 9, 12),
            Person.Sex.MALE, "bob@example.com"));
		
		persons.removeAll(newRoster);
		
		persons.stream()
			   .filter(person -> person.name == "Bob" ? persons.remove(person) : null);
		
		if(persons.contains("Bob")) {
			 System.out.println("True");
	    } else {
	  		 System.out.println("False");
	    }
	}

}
