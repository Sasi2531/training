/*
 * Problem Statement
 * 1.Sort the roster list based on the person's age in descending order using comparator
 * 
 * Requirement
 * 1.Sort the roster list based on the person's age in descending order using comparator
 * 
 * Method Signature
 * 
 * Entity
 * 1.DescendingComparator
 * 2.Comparing
 * 
 * Jobs to be Done
 * 1.Invoke the Person Class createRoster method and store it in persons list.
 * 2.Create another class comparing and implement Comparator
 * 3.Override the Comparing method 
 * 4.compare each person with age by subtracting the person age from the next person age.
 * 5.Invoke the Comparing class in Collections to sort the persons list.  
 * 6.Print each person in persons list.
 * 
 * Pseudo Code
 * List<Person> persons = Person.createRoster();
 * Collections.sort(persons ,new comparing());
 * System.out.println(persons);
 *
 */



package com.java.training.core.stream;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DescendingComparator {

	public static void main(String[] args) {
List<Person> persons = Person.createRoster();
		
		Collections.sort(persons ,new Comparing());
		
		System.out.println(persons);
		
	}

}

class Comparing implements Comparator<Person> {

	@Override
	public int compare(Person person1, Person person2) {
		return person2.getAge() - person1.getAge();
	}

}
