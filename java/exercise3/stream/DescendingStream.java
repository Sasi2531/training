/*
 * Problem Statement
 * 1.Sort the roster list based on the person's age in descending order using java.util.Stream
 * 
 * Requirement
 * 1.Sort the roster list based on the person's age in descending order using java.util.Stream
 * 
 * Method Signature
 * 
 * Entity
 * 1.DescendingStream
 * 
 * Jobs to be Done
 * 1.Invoke the Person Class createRoster method and store it in persons list.
 * 2.Sort the Persons list in Reverse Order
 *   	2.1)print each person in the list
 * 
 * Pseudo Code
 * List<Person> persons = Person.createRoster();
 * persons.stream()
 *		  .sorted(Collections.reverseOrder())
 *		  .forEach(person -> System.out.println(person));
 *
 */


package com.java.training.core.stream;
import java.util.Collections;
import java.util.List;
public class DescendingStream {
	public static void main(String[] args) {
		List<Person> persons = Person.createRoster();
		
		persons.stream()
			   .sorted(Collections.reverseOrder())
			   .forEach(person -> System.out.println(person));
	}

}
