/**
 * 
 * 1.Requirement
 *   Java program to demonstrate insertions and string buffer in tree set
 * 
 * 2.Entity
 *   TreeSetExercise
 * 
 * 3.Jobs to be Done
 *   1.Create a TreeSet
 *   2.Add values to TreeSet
 *   3.Print the Elements in TreeSet.
 * 
 * 4.Pseudo Code
 * class TreeSetExercise {
 * 
 * 		public static void main(String[] args) {
 *      
 *      	Set<String> newSet = new TreeSet<String>();
 *      
 *      	newSet.add(new StringBuffer("yoga").toString());
 *      	newSet.add(new StringBuffer("yoga balajee").toString());
 *      	newSet.add(new StringBuffer("yogi ").toString());
 *      	newSet.add(new StringBuffer("lithi").toString());
 *      
 *      	newSet.stream().forEach(x -> System.out.println(x));
 *      }
 * }
 */


package com.java.training.core.collections;
import java.util.Set;
import java.util.TreeSet;


public class TreeSetExercise {

	public static void main(String[] args) {
Set<String> newSet = new TreeSet<String>();
		
		newSet.add(new StringBuffer("sasi").toString());
		newSet.add(new StringBuffer("sasi rekha").toString());
		newSet.add(new StringBuffer("sasi ").toString());
		newSet.add(new StringBuffer("eddy").toString());
		
		newSet.stream().forEach(x -> System.out.println(x));
	}

}
