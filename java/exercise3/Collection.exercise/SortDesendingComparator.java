/*
Requirement:
    sort the roster list based on the person's age in descending order using comparator
     
Entity:
    SortDesendingComparator
     
Method Signature :
    public static void main(String[] args)
     
Jobs to be done:
    1)Create an object "roster" that access the list in Person.java
    2)Sort the roster in descending order using comparator .
    3)Convert the roster to stream.
    4)Print all the person details in stream using Iteration.
    
Pseudo code:
 
class SortDesendingComparator {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        //sort the roster using comparator
        roster.sort(Comparator.comparing(Person::getAge));
        Stream<Person> stream = roster.stream();
        stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
                + person.gender + " " + person.emailAddress));
    }
}

*/



package com.java.training.core.collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class SortDesendingComparator {

	public static void main(String[] args) {
		List<Person> roster = Person.createRoster();
        roster.sort(Comparator.comparing(Person::getAge));
        Stream<Person> stream = roster.stream();
        stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
                + person.gender + " " + person.emailAddress));
	}

}
