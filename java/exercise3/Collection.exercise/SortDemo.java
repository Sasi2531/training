/*
Requirement:
    To code for sorting the vector in the descending order.

Entity:
    VectorDemo

Method Signature:
    public static void main(String[] args)

Jobs to be done:
    1. Create an object for Vector of Integer type and add the elements to it.
    2. Now by using collections and sort method sort the elements
    3. Print the sorted vector. 
    
Pseudo code:

public class SortDemo {
    
    public static void main(String[] args) {
        Vector<Integer> vector = new Vector<>();
        vector.add(111);//add elements
        Collections.sort(vector, Collections.reverseOrder());
        System.out.println( vector);
    }

}
*/


package com.java.training.core.collections;
import java.util.Collections;
import java.util.Vector;

public class SortDemo {

	public static void main(String[] args) {
		Vector<Integer> vector = new Vector<>();
        vector.add(111);
        vector.add(999);
        vector.add(88);
        vector.add(4);
        vector.add(68);
       
        System.out.println("Vector before sorting " + vector);
        
        Collections.sort(vector, Collections.reverseOrder());
        System.out.println("The vector after sorting in descending order is " + vector);
	}

}
