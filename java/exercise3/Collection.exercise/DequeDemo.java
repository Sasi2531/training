/*
Requirement:
     Use addFirst(),addLast(),removeFirst(),removeLast(),peekFirst(),peekLast(),
     pollFirst(),poolLast() methods to store and retrieve elements in ArrayDequeue .
     
Entity:
     DequeDemo
     
Function:
     public static void main(String[] args) { }
     
Jobs to be Done;
     1)Create a deque.
     2)Add the elements to the deque.
     3)Add the first element to the deque.
     4)Print the deque.
     5)Add the last element to the deque.
     6)Print the deque.
     7)Removing the first  element in the deque.
     8)Print deque.
     9)Removing the last  element in the deque
     10)Print deque.
     11)print the first peek element.
     12)print the last peek element.
     13)print the first poll element 
     14)Print deque.
     15)print the first poll element 
     16)Print deque.
            
pseudo code:
		public class ArrayDequeDemo {
			public static void main(String[] args) {

				ArrayDeque<Integer> deque = new ArrayDeque<>();
				//add elements to the deque
				deque.addFirst();
			    System.out.println(deque);
				deque.addLast();
				System.out.println(deque);        
				deque.removeFirst(); 
				System.out.println(deque);   
				deque.removeLast();
				System.out.println(deque);
				deque.peekFirst();
				deque.peekLast();
				deque.pollFirst();
				System.out.println(deque);
				deque.pollLast();
				System.out.println(deque);
			}
		}
 * 
 */


package com.java.training.core.collections;
import java.util.ArrayDeque;

public class DequeDemo {

	public static void main(String[] args) {
		ArrayDeque<Integer> deque = new ArrayDeque<>();
        for(int number = 1 ;number <= 10 ;number++) {
        	deque.add(number);
        }
        System.out.println("The ArrayDeque " + deque);
        
        deque.addFirst(123);
        System.out.println("After adding the first element:" + deque);
        
        deque.addLast(987);
        System.out.println("After adding the last element:" + deque);
        
        deque.removeFirst();
        System.out.println("After removing the first element:" + deque);
        
        deque.removeLast();
        System.out.println("After removing the first element:" + deque);
        
        System.out.println("The first peek element:" + deque.peekFirst());
        
        System.out.println("The last  peek element:" + deque.peekLast());
        
        System.out.println("The first poll element:" + deque.pollFirst());
        System.out.println("After poll first:" + deque);
               
        System.out.println("The last poll element:" + deque.pollLast());
        System.out.println("After poll last:" + deque);


	}

}
