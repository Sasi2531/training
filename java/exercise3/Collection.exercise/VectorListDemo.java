/*
Requirement:
   code for sorting vector list in descending order.

Entity:
    VectorListDemo

Function Declaration:
   public static void main(String[] args){}
    
Jobs To Be Done:
    1)create  a vector list.
    2)add the elements to the vectorlist.
    3)print the vector list.
      3.1)sort the elements in the vector list in descending order.
    4)print the vector list.

Pseudo code:
public class VectorListDemo{
    		public static void main(String[] args) {
      		
       		VectorList<Integer> vectorList = new VectorList<Integer> ;
              //Add elements to the vector list
              System.out.println(vectorList);
              
             //sort the elements in the vector list in descending order
             
             System.out.println(vectorList);
      }      
  }
 */


package com.java.training.core.collections;
import java.util.Collections;
import java.util.Vector;


public class VectorListDemo {

	public static void main(String[] args) {
		Vector<Integer> vectorList = new Vector<Integer>();
		   for(int number = 1 ;number <= 10 ;number++) {
			    vectorList.add(number);
		   }
		   System.out.println("Before Sorting:" + vectorList);
		   Collections.sort(vectorList, Collections.reverseOrder());
		   System.out.println("After Sorting in descending order:" + vectorList) ;
		
	}

}
