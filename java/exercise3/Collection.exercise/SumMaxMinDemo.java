/*
Requirement:
    Consider a following code snippet:
        List<Integer> randomNumbers = Array.asList({1, 6, 10, 25, 78})
        - Find the sum of all the numbers in the list using java.util.Stream API
        - Find the maximum of all the numbers in the list using java.util.Stream API
        - Find the minimum of all the numbers in the list using java.util.Stream API 
Entities:
    SumMaxMinDemo

Method Signature:
    public static void main(String[] args) 

Jobs To Be Done:
    1)Create the list of type Integer.
    2)Add the elements 
    3)Print the elements using stream.
    4)Print the sum of the elements using stream.
    5)Print Maximum elements using stream.
    6)Print Minimum elements using stream.
    
Pseudo code:
class SumMaxMinDemo{
    
    public static void main(String[] args) {
        //Creating a list
        List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 25, 78);
        System.out.println("List elements are " + randomNumbers);
        //find the sum of the elements using stream and store it on number1
        System.out.println("Sum of the list elements " + number1);
        //find the Maximun elements using stream and store it on number2
        System.out.println("Maxium element in the list " + number2);
        //find the Minimum elements using stream and store it on number3
        System.out.println("Minimum element in the list " + number3);
 */

package com.java.training.core.collections;
import java.util.Arrays;
import java.util.List;

public class SumMaxMinDemo {

	public static void main(String[] args) {
		List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 25, 78);
        System.out.println("List elements are " + randomNumbers);
        int number1 = randomNumbers.stream().mapToInt(Integer::valueOf).sum();
        System.out.println("Sum of the list elements " + number1);
        int number2 = randomNumbers.stream().max(Integer::compare).get();
        System.out.println("Maxium element in the list " + number2);
        int number3 = randomNumbers.stream().min(Integer::compare).get();
        System.out.println("Minimum element in the list " + number3);

	}

}
