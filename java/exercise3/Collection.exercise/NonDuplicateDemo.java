/*
Requirement:
      Consider a following code snippet:
      List<Integer> randomNumbers = Array.asList({1, 6, 10, 1, 25, 78, 10, 25})
      - Get the non-duplicate values from the above list using java.util.Stream API
      
Entity:
     NonDulicateDemo
     
Method Signature:
     public static void main(String[] args)
          
Jobs to be done:
    1) Create list with reference to integer.
    2) Assign the given values.
    3) Create the List with reference to integer for store the non duplicate values.
          3.1) Store the elements in list which is non repeated.
    4) Print the elemenets.
    
Pseudocode: 
public class NonDuplicateDemo {
    
    public static void main(String[] args) {
        List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);
        //filter the elements which is non repeated and store it in.
        Stream<Integer> stream = withoutDuplicate.stream();
        stream.forEach(elements -> System.out.print(elements + " "));
        }
   }
        
 */


package com.java.training.core.collections;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NonDuplicateDemo {

	public static void main(String[] args) {
		List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);
        List<Integer> withoutDuplicate = randomNumbers.stream()
                .filter(element -> Collections.frequency(randomNumbers, element) == 1)
                .collect(Collectors.toList());
        Stream<Integer> stream = withoutDuplicate.stream();
        stream.forEach(elements -> System.out.print(elements + " "));

	}

}
