/*
Requirement:
 	create an array list with 7 elements, and create an empty linked list add all elements of the
    array list to linked list ,traverse the elements and display the result .

Entity:
 	ArrayToLinkedList
 
Function Declaration:
 	public static void main(String[] args)
 
Jobs To Be Done:
   1)create a array list.
   2)Add the elements to the array list.
   3)print the array list.
     3.1)create a linked list.
   4)add the elements of the array list to the linked list.
   5)Iterate the liked list till the iteration reached the length of the linked list.
     5.1)print the elements of the liked list.
 
pseudo code:
 	public class ArrayToLinkedList {

	public static void main(String[] args) {
		ArrayList<Integer> arrayList = new ArrayList<Integer>();
		
		Add elements to arrayList
		System.out.println("Array list elements are " + arrayList);
		LinkedList<Integer> linkedList = new LinkedList<Integer>();
		Add all elements of arrayList to linkedList
		System.out.println("Elements of the linked list:");
		for (int number : linkedList) {
			System.out.println(number);
		}

	}
*/



package com.java.training.core.collections;
import java.util.ArrayList;
import java.util.LinkedList;


public class ArrayToLinkedList {

	public static void main(String[] args) {
		ArrayList<Integer> arrayList = new ArrayList<Integer>();
		arrayList.add(1);
		arrayList.add(2);
		arrayList.add(3);
		arrayList.add(4);
		arrayList.add(5);
		arrayList.add(6);
		arrayList.add(7);
		System.out.println("Arraylist:" + arrayList);
		LinkedList<Integer> linkedList = new LinkedList<Integer>();
		linkedList.addAll(arrayList);
		System.out.println("Elements of the linked list:");
		for (int number : linkedList) {
			System.out.println(number);
		}
	}

}
