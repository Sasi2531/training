/*
Requirement:
    Addition,Substraction,Multiplication and Division concepts are achieved using 
    Lambda expression and functional interface.

Entity:
    LambdaInterfaceDemo

Function Declaration:
    public static void main(String[] args){} 

Jobs To Be Done:
    1)Get the two input from the user
    2)Create an interface ArithmeticOperation and create a method for input
    3)Add the two input using lambda expression
    4)Subtract the two input using lambda expression
    5)Multiply the two input using lambda expression
    6)Inside try block
        6.1)Divide the two input using lambda expression
        6.1)if the exception is thrown exception is handled.
      
Pseudo code:
    interface ArithmeticOperation {
        int input(int a,int b);
    }
    
    public class LambdaFuntionalInterface {
        public static void main(String[] args) {
            Get the two input from the user using Scanner
            Add the two input using lambda expression
            Subtract the two input using lambda expression
            Multiply the two input using lambda expression
            try {
               divide the two input using lambda expression.
            } catch(Exception exception) {
                  handle the exception
            }
       }   
    }  
*/


package com.java.training.core.collections;
import java.util.Scanner;

interface ArithmeticOperation {
	
	int values(int firstumber , int secondNumber);
}


public class LambdaInteraceDemo {

	public static void main(String[] args) {
Scanner scanner = new Scanner(System.in);
		
		System.out.print("Enter the First Number : " );
		int firstNumber = scanner.nextInt();
		
		System.out.print("Enter the Second Number : ");
		int secondNumber = scanner.nextInt();
		
		
		ArithmeticOperation addition = (int a, int b) -> (a + b);
        System.out.println("Addition = " + addition.values(firstNumber, secondNumber));

        ArithmeticOperation subtraction = (int a, int b) -> (a - b);
        System.out.println("Subtraction = " + subtraction.values(firstNumber, secondNumber));

        ArithmeticOperation multiplication = (int a, int b) -> (a * b);
        System.out.println("Multiplication = " + multiplication.values(firstNumber, secondNumber));
        
        try {
        	
        	ArithmeticOperation division = (int a, int b) -> (a / b);
            System.out.println("Division = " + division.values(firstNumber, secondNumber));
        	
        } catch (Exception exception) {
        	System.out.println("correct values must be inserted to perform division");
        }
           scanner.close();
		

	}

}
