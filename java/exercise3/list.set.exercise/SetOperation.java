/*
Requirement:+ Create a set
           => Add 10 values
           => Perform addAll() and removeAll() to the set.
           => Iterate the set using 
                  - Iterator
                  - for-each
           + Explain the working of contains(), isEmpty() and give example.
           
Entities:
  SetOperation
  
Function Declaration:
     public static void main(String[] args)
     
Jobs To Be Done:1.consider the program
                2.A set is created named set
                3.Values are added into the set by using add method 
                4.Another set is created named set1
                5.values of set is added to set1 by using addAll method
                6.set1 is printed
                7.The values in the set1 is removed if the value is present in set by using removeAll
                8.The set is iterated using iterator and it is printed
                9.The set is iterated using for each loop and it is printed
                10.if 1 is present in set it will print as 1 is present,the condition is checked in if using contains.
                11.otherwise it will print as "1 is not present" where print statement is given inside the else block.
                12.Then the set is empty or not is checked using isEmpty
                13.if the set is empty it will print as set is empty
                14.otherwise it will print as set is not empty 
                    
 */



package com.java.training.core.list;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


public class SetOperation {

	public static void main(String[] args) {
Set<Integer> set = new HashSet<Integer>(); 
		
		//adding elements to the set
		for(int number = 0 ;number < 11 ;number++) {
			set.add(number+10);
		}
		
		Set<Integer> set1 = new HashSet<Integer>(); 
		
		//addall
		set1.addAll(set);
		System.out.println(set1);
		
		//removeall
		set1.removeAll(set);
		System.out.println(set1);
		
		//using Iterator
		System.out.println("Iterating list using iterator");
		Iterator number = set.iterator();
		while(number.hasNext()) {
			System.out.println(number.next());
		}
		
		//iterate using for each loop
		System.out.println("Iterating list using for each");
		for(int digit : set) {
			System.out.println(digit) ;
		}
		
		//contains
		if(set.contains(1)) {
			System.out.println("1 is present");
		} else {
			System.out.println("1 is not present");
		}
		
		//isempty
		if(set1.isEmpty()) {
			System.out.println("set is empty");
		} else {
			System.out.println("set is not empty");
		}
		
        

	}

}
