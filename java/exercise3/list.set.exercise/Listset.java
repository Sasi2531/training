/*
Requirement:
  To Create a list
    1. Add 10 values in the list
    2. Create another list and perform addAll() method with it
    3. Find the index of some value with indexOf() and lastIndexOf()
    4. Print the values in the list using 
        - For loop
        - For Each
        - Iterator
        - Stream API


Entity:
  A public class named List is used.

Function Declaration:
  No function is declared.
Jobs to be done:
  1.Create a package
  2.import util packages.
  3.Declare the class ListSet
  4.Create an empty list and add the elements to it and print it.
  5.Using addAll() function add the elements of list b to list
  6.Using indexOf() and LastIndexOf() method find the index of the elements
  7.Using,for loop,enhanced for loop iterator and stream to print the elements of the list.


*/

package com.java.training.core.list;
import java.util.*;

public class Listset {

	public static void main(String[] args) {
		ArrayList<String> list=new ArrayList<String>();
		list.add("Mango");
		list.add("Apple");
		list.add("Orange");
		list.add("Grapes");
		list.add("Banana");
		list.add("Papaya");
		list.add("Pineapple");
		list.add("Watermelon");
		list.add("Cherry");
		list.add("Plums");
		ArrayList<String> a=new ArrayList<String>();
		a.add("Strawberry");
		a.add("Kiwi");
		list.addAll(a);
		System.out.println(" "+list);
        int i1=list.lastIndexOf("Banana");
        System.out.println(i1);
        int i2=list.indexOf("Papaya");
        System.out.println(i2);
        for(int i=0;i<list.size();i++) {
        	System.out.println(list.get(i));
        }
        Iterator<String> iteratorList= list.iterator();
        while(iteratorList.hasNext()) {
        	System.out.println("The elements are :" + " " + iteratorList.next());
        }
        list.stream().forEach((element)-> System.out.println(element));

	}

}
