/*
Requirement:
  To Create a list
   1. Convert the list to a set
   2. Convert the list to a array

Entity:
  A public class named List is used.

Function Declaration:
  No function is declared.
Jobs to be done:
  1.Create a package
  2.import util packages.
  3.Declare the class ListConversion.
  4.Create an empty list and add the elements to it and print it.
  5.Convert list to set.
  6.Convert list to array.


*/


package com.java.training.core.list;
import java.util.*;

public class ListConversion {

	public static void main(String[] args) {
		ArrayList<String> list=new ArrayList<String>();
		list.add("Mango");
		list.add("Apple");
		list.add("Orange");
		list.add("Grapes");
		list.add("Banana");
		list.add("Papaya");
		list.add("Pineapple");
		list.add("Watermelon");
		list.add("Cherry");
		list.add("Plums");
		ArrayList<String> a=new ArrayList<String>();
		a.add("Strawberry");
		a.add("Kiwi");
		list.addAll(a);
		String[] myArray=new String[list.size()];
		list.toArray(myArray);
		for(int i=0;i<myArray.length;i++)
		{
			System.out.println("Element at index "+ i + " "+myArray[i]);
		}
		Set<String> s= new HashSet<String>(list);
        System.out.println("Set is :" + s);

	}

}
