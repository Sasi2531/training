/*

1.Requirements:
    - Program to number of files in a directory and number of directories in a directory.
    
2.Entities:
    - FileDirectoryCount
    
3.Methodsignature:
   - public static void main(String[] args)
  
4.Jobs to be done:
    1.Create File class and pass the file path in File class constructor argument.
    2.Invoke list method and length to find the number files in directory.
    3.Print the number files in directory.
    
Pseudo Code:
''''''''''''

public class FileDirectoryCount {
	public static void main(String args[]) {
		File directory = new File("C:\Users\yogabalajee\Downloads\advance.nio\advance.nio");
		int fileCount = directory.list().length;
		System.out.println("File Count:" + fileCount);
	}
}
*/


package com.java.training.core.nio.advance;
import java.io.File;
public class FileDirectoryCount {

	public static void main(String[] args) {
		File directory = new File("C:\\1Temp\\training\\java\\exercise3\\nio.exercise\\advance");
		int fileCount = directory.list().length;
		System.out.println("File Count:" + fileCount);
	}

}
