/*

1.Requirements:
    - Program to check if path is file or directory
    
2.Entities:
    - FileOrDiretory
    
3.Methodsignature:
   - public static void main(String[] args)
  
4.Jobs to be done:
    1.Get the file using File class passing parameter in File class.
    2.Invoke isFile method to check path is file or directory 
       2.1)Check and Print file or directory.
    
    
Pseudo Code:
''''''''''''
public class FileOrDiretory {
	public static void main(String[] args) {
		File file = new File("C:\1Temp\training\java\exercise3\nio.exercise\advance\sample nio.txt");
		if (file.isFile()) {
			System.out.println("The Given path is file");
		} else {
			System.out.println("The Given path is directory");
		}
		
	}
}

*/


package com.java.training.core.nio.advance;
import java.io.File;
public class FileOrDiretory {

	public static void main(String[] args) {
		File file = new File("C:\\1Temp\\training\\java\\exercise3\\nio.exercise\\advance\\sample nio.txt");
		if (file.isFile()) {
			System.out.println("The Given path is file");
		} else {
			System.out.println("The Given path is directory");
		}
	}

}
