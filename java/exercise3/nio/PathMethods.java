/*
1.Requirements:
    - Program to demonstrate toAbsolutePath(), normalize(), getName(), getFileName() and getFileCount().
    
2.Entities:
    - PathMethods
    
3.Methodsignature:
   - public static void main(String[] args)
  
4.Jobs to be done:
    1.Get the file path using Paths class get method pass file name.
          1.1)Get absolut path using toAbsolutePath method.
          1.2)Print the absolute path.
    2.Get the file path using Paths class get method pass file path.
          2.1)Get normalize path using normalize method.
          2.2)Print the normalize path.
    3.Get the file path using Paths class get method pass file path.
          3.1)Get get index file Name using getName method.
          3.2)Print the specified index file path.
    4.Get the file path using Paths class get method pass file path.
          4.1)Get get file Name using getFileName method.
          4.2)Print the file path.
    5.Get the file path using Paths class get method pass file path.
          5.1)Get get the no of in directory using size method with length.
          5.2)Print the no of files in directory  
          
Pseudo Code:

public class PathMethods {
	public static void main(String[] args) {

		Path path = Paths.get("PathMethods.java");
		Path absPath = path.toAbsolutePath();
		System.out.println("Absolute Path: " + absPath);
		
		Path normalPath = Paths.get("C:/Users/santh/eclipse-workspace/JavaEE-Demo/nio/PathMethods.java");
		Path normalizedPath = normalPath.normalize();
		System.out.println("Normalized Path : " + normalizedPath);

		Path getNamePath = Paths.get("C:/Users/santh/eclipse-workspace/JavaEE-Demo/nio/");
		Path indexpath = getNamePath.getName(4);
		System.out.println("Index 4: " + indexpath);

		Path pathGet = Paths.get("C:/Users/santh/eclipse-workspace/JavaEE-Demo/nio/PathMethods.java");
		Path fileName = pathGet.getFileName();
		System.out.println("FileName: " + fileName.toString());

		File directory = new File("C:/Users/santh/eclipse-workspace/JavaEE-Demo/nio");
		int fileCount = directory.list().length;
		System.out.println("File Count:" + fileCount);
	}
}


*/



package com.java.training.core.nio;
//Java program to demonstrate 
//java.nio.file.Path.toAbsolute() method 

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
public class PathMethods {

	public static void main(String[] args) {
		// create object of Path
				Path path = Paths.get("PathMethods.java");
				// call toAbsolutePath() to get
				// absolute path
				Path absPath = path.toAbsolutePath();
				// print absolute path
				System.out.println("Absolute Path: " + absPath);
				
				// create object of Path
				Path normalPath = Paths.get("C:\\1Temp\\training\\java\\exercise3\\nio.exercise\\PathMethods.java");
				// normalize the path
				Path normalizedPath = normalPath.normalize();
				// print normalized path
				System.out.println("Normalized Path : " + normalizedPath);
				// call getName(int i) to get the
				// element at index i
				
				// create object of Path
				Path getNamePath = Paths.get("C:\\1Temp\\training\\java\\exercise3\\nio.exercise");		
				Path indexpath = getNamePath.getName(4);
				// print ParentPath
				System.out.println("Index 4: " + indexpath);
				
				// create object of Path
				Path pathGet = Paths.get("C:\\1Temp\\training\\java\\exercise3\\nio.exercise\\PathMethods.java");
				// call getFileName() and get FileName path object
				Path fileName = pathGet.getFileName();
				// print FileName
				System.out.println("FileName: " + fileName.toString());

				
				File directory = new File("C:\\1Temp\\training\\java\\exercise3\\nio.exercise");
				int fileCount = directory.list().length;
				System.out.println("File Count:" + fileCount);

	}

}
