/*
NIO PIPE:
=========
1) Write a program to write and read the string to a channel using buffer(to demonstrate Pipe)

1.Requirements:
    - Program to write and read the string to a channel using buffer.
    
2.Entities:
    - ChannelBuffer
    
3.Methodsignature:
   - public static void main(String[] args)
  
4.Jobs to be done:
    1.Store the store to write in file.
    2.Store file path and file name in strings.
    3.File class check file exist or not.
    4.Wirte string in file uisng for loop. 
        4.1)Invoke toCharArray method store the char.
    
Pseudo Code:

public class ChannelBuffer {
  public static void main(String[] args) throws IOException {
    String phrase = new String("This content writtern by Channel Buffer");
    String dirname = "C:\Users\yogabalajee\eclipse-workspace\javaee-demo\src\com\java\training\nio"; 
    String filename = "destination.txt"; 
    File dir = new File(dirname); 
    File aFile = new File(dir, filename); 
    FileOutputStream outputFile = null; 
    try {
      outputFile = new FileOutputStream(aFile, true);
    } catch (FileNotFoundException e) {
      e.printStackTrace(System.err);
    }
    FileChannel outChannel = outputFile.getChannel();
    ByteBuffer buf = ByteBuffer.allocate(1024);
    for (char ch : phrase.toCharArray()) {
      buf.putChar(ch);
    }
}

*/


package com.java.training.core.nio;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
public class ChannelBuffer {

	 public static void main(String[] args) throws IOException {
		    String phrase = new String("\nThis content writtern by Channel Buffer");
		    String dirname = "C:\\Users\\yogabalajee\\eclipse-workspace\\javaee-demo\\src\\com\\java\\training\\nio"; 
		    String filename = "destination.txt"; 
		    File dir = new File(dirname); 
		    File aFile = new File(dir, filename); 
		    FileOutputStream outputFile = null; 
		    try {
		      outputFile = new FileOutputStream(aFile, true);
		      System.out.println("File stream created successfully.");
		    } catch (FileNotFoundException e) {
		      e.printStackTrace(System.err);
		    }
		    FileChannel outChannel = outputFile.getChannel();
		    ByteBuffer buf = ByteBuffer.allocate(1024);
		    for (char ch : phrase.toCharArray()) {
		      buf.putChar(ch);
		    }
		    buf.flip(); 
		    System.out.println("Buffer contents written to file.");
		    outChannel.write(buf);
		    outputFile.close(); 
		  }
}
