/*
2.Write a code to rounding the value to maximum of 3 digits by setting maximum and minimum Fraction digits.

 
1.Requirement:
     - Program to rounding the value to maximum of 3 digits by setting maximum and minimum Fraction digits.
     
2.Entity:
     - NumberFraction
     
3.Function Declaration:
  	 - public static void main(String[] args)
  	 
4.Jobs To Be Done:
  	1.Invoke NumberFormat class getInstance method and store it in numberFormat.
  	2.Set Minimun and Maximun fraction digits using
              * setMinimumFractionDigits and
              * setMaximumFractionDigits method.
    3.Invoke format method with float value to print. 
    
PseudoCode:

import java.net.*;

public class NumberFraction {
	public static void main(String[] args) {
		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setMinimumFractionDigits(1);
		numberFormat.setMaximumFractionDigits(3);
		System.out.println(numberFormat.format(21.3243f));
	}
}

*/ 


package com.java.training.core.internationalization;
import java.text.NumberFormat;
public class NumberFraction {

	public static void main(String[] args) {
		NumberFormat numberFormat = NumberFormat.getInstance();
		//Set Minimun fraction figits
		numberFormat.setMinimumFractionDigits(1);
		//Set Maximun fraction figits
		numberFormat.setMaximumFractionDigits(3);
		System.out.println(numberFormat.format(21.3243f));
	}

}
