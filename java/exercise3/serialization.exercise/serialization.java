/*
Requirements:
1)What is serialization
2)What is need for serialization
3)What happens if one of the members in a class does not implement Serializable interface?
*/
Explanation:
  - To serialize an object means to convert its state to a byte stream so that the byte stream 
    can be reverted back into a copy of the object. 
  - We need serialization because the hard disk or network infrastructure are hardware component 
    and we cannot send java objects because it understands just bytes and not java objects
  - When we  try to serialize an object which implements Serializable interface, incase if the 
    object includes a reference of an non serializable object then NotSerializableException will be thrown.