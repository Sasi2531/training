/*
Requirements:
  create a program for url class  in networking
  
Entities:
  The class named as UrlClass.
  
Function Declaration:
  public static void main(String[] args)
  
Jobs to be done:
   1.Create a class called UrlClass 
   2.In mainmethod,try catch block is used.
   3.create a url path,and set the  path name.
      3.1 getProtocol() method,used to get protocol name of website through path name
      3.2 getHost()method,used to  get hostname 
      3.3 getPort() ,used to get the portnumber
      3.4 getFile(),used to get the file name
   4.print the results.
   
pseudocode:
public class UrlClass {
	 
	
	public static void main(String[] args){  
	try{  
	URL url=new URL("http://www.javatpoint.com/java-tutorial");  
	  
	System.out.println("Protocol: "+url.getProtocol());  
	System.out.println("Host Name: "+url.getHost());  
	System.out.println("Port Number: "+url.getPort());  
	System.out.println("File Name: "+url.getFile());  
	  
	}catch(Exception e){System.out.println(e);}  
	}  
	}  
*/


package com.java.training.core.networking;
import java.net.*; 

public class UrlClass {

	public static void main(String[] args) {
		try{  
			URL url=new URL("http://www.javatpoint.com/java-tutorial");  
			  
			System.out.println("Protocol: "+url.getProtocol());  
			System.out.println("Host Name: "+url.getHost());  
			System.out.println("Port Number: "+url.getPort());  
			System.out.println("File Name: "+url.getFile());  
			  
			}catch(Exception e){System.out.println(e);}  

	}

}
