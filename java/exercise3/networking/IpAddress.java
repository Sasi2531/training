/*
Requirements:
   Find out the IP address and host name of local computer
   
Entities:
 The class named as IpAddress	
 	
Function Declaration:
  public static void main(String[] args)
  		
JobsToBeDone:
  1.Create a class named IpAddress
  2.Inside main method, try and catch block is used
  	2.1.Use InetAddress method to get ipaddress and host name.
  3.Print the values.
  
PseudoCode:
public class IpAddress{
   public static void main(String[] args){
      try{
        //IP address and host name using methods      }
      catch (UnknownHostException e){
       // print error message   }
   }
}
*/



package com.java.training.core.networking;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class IpAddress {

	public static void main(String[] args) {
		try{
	         InetAddress my_address = InetAddress.getLocalHost();
			 System.out.println("LocalHost is : " + my_address);
	         System.out.println("The IP address is : " + my_address.getHostAddress());
	         System.out.println("The host name is : " + my_address.getHostName());
			 
			 my_address = InetAddress.getByName("google.com.sa");
			 System.out.println("Google inetaddress is : "+ my_address);
	      }
	      catch (UnknownHostException e){
	         System.out.println( "Couldn't find the local address.");
	      }

	}

}
