/*
Requirements:
  create a program using HttpURLConnection in networking.
  
Entities:
  The class named as HttpUrlConnection
  
Function Declaration:
  public static void main(String[] args)
  
Jobs to be done:
   1.Create a class calledHttpUrlConnection
   2.In mainmethod,try catch block is used.
   3.create a url path,and set the  path name.
        3.1 create a object as huc and openConnection() method is used to open the connection.
        3.2 Using forloop,to iterate the values and print
        3.3 disconnect() method is used to diconnect the connection.
   4.print the result
   
pseudocode:
public class HttpUrlConnection{    
public static void main(String[] args){    
try{    
URL url=new URL("https://matlab.mathworks.com/");    
HttpURLConnection huc=(HttpURLConnection)url.openConnection();  
for(int i=1;i<=8;i++){  
System.out.println(huc.getHeaderFieldKey(i)+" = "+huc.getHeaderField(i));  
}  
huc.disconnect();   
}catch(Exception e){System.out.println(e);
}    
}    
}
*/


package com.java.training.core.networking;
import java.net.*;  

public class HttpUrlConnection {

	public static void main(String[] args) {
		try{    
			URL url=new URL("https://matlab.mathworks.com/");    
			HttpURLConnection huc=(HttpURLConnection)url.openConnection();  
			for(int i=1;i<=8;i++){  
			System.out.println(huc.getHeaderFieldKey(i)+" = "+huc.getHeaderField(i));  
			}  
			huc.disconnect();   
			}catch(Exception e){System.out.println(e);
			}    
	}

}
