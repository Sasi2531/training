/*
Requirements:
  Difference between replaceAll() and appendReplacement()
*/
Explanation:
- replaceAll() method of this (Matcher) class accepts a string value, replaces all the matched 
subsequences in the input with the given string value and returns the result.
- appendReplacement() method of this (Matcher) class accepts a StringBuffer object and a String 
 (replacement string) as parameters and, appends the input data to the StringBuffer object, replacing 
 the matched content with the replacement string.