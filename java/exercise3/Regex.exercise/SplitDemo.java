/*
Requirement:
   To split any random text using any pattern you desire.
   
Entities:
   The class named as SplitDemo.
   
Functiondeclaration:
    public static void main(String args[])
    
Jobs to be done:
   1.create and import the package
   2.create a class called SplitDemo 
   3.In main method,create a string and assign the values for it
     3.1 create a pattern and split() method is used to split the text values
     3.2 In forloop,the variable result values are assigned to temp which the type as String
   4.print the result.
   
pseudocode:
public class SplitDemo {
	
	 
	    public static void main(String args[]) 
	    { 
	        String text = "geeks1for2geeks3"; 
	  
	        
	        String delimiter =  "\\d"; 
	        Pattern pattern = Pattern.compile(delimiter, 
	                                        Pattern.CASE_INSENSITIVE); 
	  
	      
	        String[] result = pattern.split(text); 
	  
	  
	        for (String temp: result) 
	            System.out.println(temp); 
	    } 
	} 

*/

package com.java.training.core.regex;
import java.util.regex.Pattern; 

public class SplitDemo {

	public static void main(String[] args) {
		String text = "geeks1for2geeks3"; 
		  
        
        String delimiter =  "\\d"; 
        Pattern pattern = Pattern.compile(delimiter, 
                                        Pattern.CASE_INSENSITIVE); 
  
      
        String[] result = pattern.split(text); 
  
  
        for (String temp: result) 
            System.out.println(temp); 
	}

}
