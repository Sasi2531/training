package com.java.training.core.regex;
import java.util.regex.*; 


public class GroupRegex {

	public static void main(String[] args) 
		 { 
			  
		        // Get the regex to be checked 
		        String regex = "(people)"; 
		  
		        // Create a pattern from regex 
		        Pattern pattern 
		            = Pattern.compile(regex); 
		  
		        // Get the String to be matched 
		        String stringToBeMatched 
		            = "GeeksForGeeks"; 
		  
		        // Create a matcher for the input String 
		        Matcher matcher 
		            = pattern 
		                  .matcher(stringToBeMatched); 
		  
		        // Get the current matcher state 
		        MatchResult result 
		            = matcher.toMatchResult(); 
		        System.out.println("Current Matcher: "
		                           + result); 
		  
		        while (matcher.find()) { 
		            // Get the group matched using group() method 
		            System.out.println(matcher.group()); 
		        } 
	}

}
