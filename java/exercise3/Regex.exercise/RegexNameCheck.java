/*Requirement:
    To check the name which starts with M and should be 6 characters.

Entity:
    RegexNameCheck

Function Declaration:
    public static void main(String[] args)

Jobs To Be Done:
    1)Getting the input from the user.
    2)check the input is null or space or numbers or any special character if these
     conditions are not satisfied.
        2.1) throw exception the input should not be contain null or number or any 
             special character.
    3)create a pattern 
        3.1) that checks whether the string starts with 'm'(m is case insensitive )
        3.2) check the string is of 6 characters
    4) check  whether the input matches the pattern
        4.1) if it match print "valid input"
        4.2) if it does not match print "invalid input"
    


Pseudo code:

class RegexDemo {
	public static void main(String[] args) {
    	Scanner scanner = New Scanner(System.in);
    	String name = scanner.nextline();
    	//Get the input from user using scanner
    	
    	if((name == null) || (name.is Number()) || (name.isSpecialCharacter())) {
    		throw new Exception("the input should not be contain null or number or any 
    		special character");
    	}
    	//Pattern pattern (or) String pattern
    	if (name == pattern) {
    		System.out.println("Valid input");
    	} else {
    		System.out.println("Invalid input");
    	}
    }
}
*/



package com.java.training.core.regex;
import java.util.regex.*;
public class RegexNameCheck {

	public static void main(String[] args) {
		String name = "mallika";
		Pattern p = Pattern.compile("[M|m][a-z|A-Z]{5}");
		Matcher m = p.matcher(name);
		if(m.find()) {
			System.out.println(name + " is valid Name");
		}
		else {
			System.out.println(name + " is not valid Name");
		}

	}

}
