/*
Requirements:
   To read any textfile using bufferedReader and print the content of the file.
   
Entities:
  The class named as BufferedReader1

FunctionDeclaration:
   public static void main(String[] args)

Jobs to be done:
   1.cretae and import a package.
   2.create a class called BufferedReader1
   3.In main method,try and catch block is used.
     3.1 In First,Set the path to read the file and declare the variable as i
     3.2 In while condition,reading the file until it gets false and print the result
   4.After read the file,Use close()method is used to close the file.
   
Pseudocode:
public class BufferedReader1 {

	public static void main(String[] args) {
          // create try and catch block
		 try{    
		    FileInputStream fin=new FileInputStream("C:\\data\\byteoutput1.txt");    
		    BufferedInputStream bin=new BufferedInputStream(fin);  //set the path to read the file  
		    int i;    
		    while((i=bin.read())!=-1){    
		     System.out.print((char)i);    
		    }    
		    bin.close();    
		    fin.close();    
		  }catch(Exception e)
		 {
			  System.out.println(e);
		 }      
	}

}
*/

package com.java.training.core.IO;
import java.io.*; 

public class BufferedReader1 {

	public static void main(String[] args) {
		 try{    
			    FileInputStream fin=new FileInputStream("C:\\data\\byteoutput1.txt");    
			    BufferedInputStream bin=new BufferedInputStream(fin);    
			    int i;    
			    while((i=bin.read())!=-1){    
			     System.out.print((char)i);    
			    }    
			    bin.close();    
			    fin.close();    
			  }catch(Exception e)
			 {
				  System.out.println(e);
			 }      

	}

}

