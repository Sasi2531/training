/*
Requirements:
To read the file using Reader
Entities:
The class named as Reader
Functiondeclaration:
   public static void main(String[] args)
Jobs to be done:
1.create and import the package.
2.create class named as Reader
3.In main method,throws Exception is used
  3.1 Set the path of the file and declare the variable as i
  3.2 In while loop,read the file until loop gets false
  3.3 Print the result
4.Use close() method,to close the readingfile.
Pseudocode:
public class Reader {
           // throws Exception
	public static void main(String[] args)throws Exception{    
		          FileReader fr=new FileReader("C:\\data\\write.txt"); //set the path to read the file   
		          int i;    
		          while((i=fr.read())!=-1)    
		          System.out.print((char)i);    
		          fr.close();    
		    }    
}
*/


package com.java.training.core.IO;
import java.io.FileReader;  

public class Reader {

	public static void main(String[] args)throws Exception {
		FileReader fr=new FileReader("C:\\data\\write.txt");    
        int i;    
        while((i=fr.read())!=-1)    
        System.out.print((char)i);    
        fr.close();    
	    }

}
