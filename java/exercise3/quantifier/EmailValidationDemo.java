/*
Requirement:
   Write a program for email-validation.

Entity:
  -EmailValidationDemo

Method Signature:
  -public static void main(String[] args)

Jobs to be done:
   1) Get the input from user using Scanner.
   2) Create a pattern that should have some conditions
   3) Create a matcher that tries to match input and pattern
   4) If matches
       4.1) Display the input is a valid email otherwise invalid.

Pseudo Code:
public class EmailValidationDemo {

    public static void main(String[] args) {

        // getting the input from user
        Scanner scanner = new Scanner(System.in);
        String inputMail = scanner.nextLine();

        // creating a pattern to validate user input
        Pattern pattern = Pattern
                .compile("^[a-zA-Z\\d._]{6,}+@[a-z.]+\\.[a-z]{2,}$");

        // checking matcher matches the input
        Matcher matcher = pattern.matcher(inputMail);

        // if matches
        if (matcher.find()) {
            System.out.println("It is a valid email.");
        } else {
            System.out.println("Invalid email.");
        }
    }

}
*/

package com.java.training.core.quantifier;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidationDemo {

	public static void main(String[] args) {
		 // getting the input from user
        @SuppressWarnings("resource")
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter your email address: ");
        String inputMail = scanner.nextLine();

        // creating a pattern to validate user input
        Pattern pattern = Pattern
                .compile("^[a-zA-Z\\d._]{6,}+@[a-z.]+\\.[a-z]{2,}$");

        // checking matcher matches the input
        Matcher matcher = pattern.matcher(inputMail);

        // if matches
        if (matcher.find()) {
            System.out.format("'%s'" + " is a valid email.", matcher.group());
        } else {
            System.out.println("Invalid email.");
        }

	}

}
