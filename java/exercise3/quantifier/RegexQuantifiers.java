/*
Requirement:
   Write a program for java regex quantifier

Entity:
   -RegexQuantifiers

Method Signature:
   -public static void main(String[] args)

Jobs to be done:
    1) Get the input string.
    2) Create a pattern that should have some conditions
    3) Create a matcher that tries to match input and pattern
    4) Find the word is present
        4.1) If present display the position of pattern

Pseudo Code:
public class RegexQuantifiers {

    public static void main(String[] args) {
        String sentence = "Albert Einstein was one of the " +
                "greatest scientist of all the time.";

        // creating a pattern
        Pattern pattern = Pattern.compile("the+");

        // checking the pattern conditions matches the string
        Matcher matcher = pattern.matcher(sentence);

        // find the occurrance of the pattern
        while (matcher.find()) {
            System.out.println(matcher.group() + " found at " + matcher.start());
        }
    }

}
*/
//Quantifiers:
//    X*        Zero or more occurrences of X
//    X?        Zero or One occurrences of X
//    X+        One or More occurrences of X
//    X{n}      Exactly n occurrences of X 
//    X{n, }    At-least n occurrences of X
//    X{n, m}   Count of occurrences of X is from n to m

package com.java.training.core.quantifier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexQuantifiers {

	public static void main(String[] args) {
		 String sentence = "Albert Einstein was one of the " +
	                "greatest scientist of all the time.";

	        // creating a pattern
	        Pattern pattern = Pattern.compile("the+");

	        // checking the pattern conditions matches the string
	        Matcher matcher = pattern.matcher(sentence);

	        // find the occurrance of the pattern
	        while (matcher.find()) {
	            System.out.println("'" + matcher.group() + "'" + " found at " +
	                    matcher.start());
	        }

	}

}
