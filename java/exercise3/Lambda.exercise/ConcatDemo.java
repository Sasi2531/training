/*
Requirement:
    To write a Lambda expression program with a single method interface to concatenate two strings.

Entity:
    public class ConCatDemo.
    interface ConCationDemo.

Function Declaration:
    public String conCat(String firstString, String secondString)
    public static void main(String[] args)

Jobs to be done:
    1. Create a package com.java.training.core.lambda
    2. Create a interface ConCationDemo.
    3. Declare the class ConcatDemo as public.
    4. Declare a method public String conCat(String firstString, String secondString)
    5. Under a main method Create a lambda expression and call the method and print the result. 
*/


package com.java.training.core.lambda;
interface ConcationDemo {
    
    public String conCat(String firstString, String secondString);
}

public class ConcatDemo {

	public static void main(String[] args) {
		ConcationDemo conCatString = (firstString, secondString) -> (firstString + secondString);
        System.out.println(conCatString.conCat("Sasi", "Eddy"));
	}

}
