/*
Requirement:
      demonstrate java program to working of map interface using( put(), remove(), 
      booleanContainsValue(), replace() )
      
Entity:
      MapInterfaceDemo
      
Function declaration:
     public static void main(String[] args) 
     
Jobs to be done:
      1)Create a treemap
      2)add elements to the treemap.
      3)print the treemap.
      4)Remove the key value pair.
      5)print the treemap.
      6)check value is present and print the result as boolean
      7)Replace particular value
      8)print the treemap
      
Pseudo code:
		public class MapInterfaceDemo {

			public static void main(String[] args) {
				//create a new treemap
				//add elements to the treemap
				treemap.remove();
				System.out.println(treemap);
				System.out.println(treemap.containsValue());
				System.out.println(treemap);
				treemap.replace();
				System.out.println(treemap);		
			}
		}
*/


package com.java.training.core.map;
import java.util.TreeMap;

public class MapWorkingDemo {

	public static void main(String[] args) {
		TreeMap<Integer, String> treeMap = new TreeMap<>();
		treeMap.put(1 , "ones");
		treeMap.put(2 , "two");
		treeMap.put(3 , "three");
		treeMap.put(4 , "four");
		treeMap.put(5 , "five");
		System.out.println("treemap:" + treeMap);
		treeMap.remove(1);
		System.out.println("After removing:" + treeMap);
		System.out.println(treeMap.containsValue("three"));
		treeMap.replace(1, "one");
		System.out.println("After replacing the value of 1:" + treeMap);

	}

}
