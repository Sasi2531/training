/*
 *  1)Requirements
 * 		Write a Java program to test if a map contains a mapping for the specified key
 * 
 * 2)Entity
 * 		ValuesMap Class
 * 
 * 3)Jobs to be done
 * 		1.Created a new HashMap called newMap and added some values.
 * 		2.Then getting value from the user to check from the HashMap.
 * 		3.Map Interface has a method called containsValue method to find the value present in the Map.
 * 		4.If the value got from the user is present in the Map it says Value Exist, else Does not Exists 
 *   	is printed.
 * 
 * 4)Pseudo code
 * 		public class ValuesMap {
	
			public static void main(String[] args) {
			
				Map<Integer,String> newMap = new HashMap<Integer,String>();
				String inValue = scan.nextLine();
				
				if(newMap.containsValue(inValue)) {
					
				} 
				else {
				
				}
				scan.close();
 *
 */


package com.java.training.core.map;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class ValuesMap {

	public static void main(String[] args) {
		Scanner scan =new Scanner(System.in);
		// New HashMap
		Map<Integer,String> newMap = new HashMap<Integer,String>();
		//Adding Values to the Map
		newMap.put(1, "cse");
		newMap.put(2, "eee");
		newMap.put(3, "ece");
		newMap.put(4, "Mech");
		newMap.put(5, "civil");
		newMap.put(6, "Ai");
		newMap.put(7, "IT");
		
		System.out.println("Enter the Value to Find from the Map!");
		String inValue = scan.nextLine();
		//containsValue method to find the value.
		if(newMap.containsValue(inValue)) {
			System.out.println("Value Exists");
		} else {
			System.out.println("Value Does not Exists");
		}
		scan.close();

	}

}
