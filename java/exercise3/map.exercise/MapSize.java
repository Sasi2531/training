/*
 * 1)Requirements
 * 	Count the size of mappings in a map?
 * 
 * 2)Entity
 * 	MapSize Class
 * 
 * 3)Jobs to be done
 * 		1.Created a new HashMap named newMap and added some values in to it.
 * 		2.Now the Size of the Map is printed using the size() method provided by the Map Interface.
 *		3.Here Map newMap is passed with the size() method to get the size of HashMap
 * 		4.Created a Flag called count of Integer type.
 * 		5.Inside for loop creating a Map called prepMap with Map.Entry to store the key and values of the Map.
 * 		6.Now checking condition, while the prepMap.getValue is not equal to zero then the count flag should 
 *   	increase by 1 and after the loop finishes.The Count value is printed.
 * 
 * 4)Pseudo code
 * 		public class MapSize {
	
	public static void main(String[] args)
	//New HashMap
		Map<Integer,Integer> newMap = new HashMap<Integer,Integer>();
		//Values added to the Map
		newMap.put
		
		//Printing the size of the Map using size() method..
		System.out.println(newMap.size())
		
		//Printing the size of the Map using for loop with entrySet.
		Integer count = 0;
		for(Map.Entry<Integer, Integer> perpMap : newMap.entrySet()) {
			while(perpMap.getValue() != 0) {
				count += 1;
 *
 */


package com.java.training.core.map;
import java.util.HashMap;
import java.util.Map;

public class MapSize {

	public static void main(String[] args) {
		//New HashMap
				Map<Integer,Integer> newMap = new HashMap<Integer,Integer>();
				//Values added to the Map
				newMap.put(1,10);
				newMap.put(2,20);
				newMap.put(3,30);
				newMap.put(4,40);
				
				//Printing the size of the Map using size() method..
				System.out.println(newMap.size());
				
				//Printing the size of the Map using for loop with entrySet.
				Integer count = 0;
				for(Map.Entry<Integer, Integer> perpMap : newMap.entrySet()) {
					while(perpMap.getValue() != 0) {
						count += 1;
					}
				}
				System.out.println(count);

	}

}
