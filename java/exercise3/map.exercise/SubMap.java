/*
 * 1)Requirements
 * 		Write a Java program to get the portion of a map whose keys range 
 *   	from a given key to another key
 *   
 * 2)Entity
 * 		SubMap Class
 * 
 * 3)Jobs to be done
 * 		1.Created a new TreeMap called oldMap in which the values can be accessed dynamically.
 * 		2.Added values to the oldMap using put method.
 * 		3.Getting two user value access using subMap. 
 * 		4.Accessing the oldMap value using subMap function 
 * 		5.At the last line the oldMap has passed with subMap and passing the starting and ending value
 * 		6.Passing the startVal and endVal got from the user to the subMap function and print the values.
 * 
 * 4)Pseudo code
 *		public class SubMap {
	
			public static void main(String[] args) {
			
			SortedMap<Integer,String> oldMap = new TreeMap<Integer,String>();
			
			int startVal = scan.nextInt();
			int endVal = scan.nextInt();
 */



package com.java.training.core.map;
import java.util.Scanner;
import java.util.SortedMap;
import java.util.TreeMap;
public class SubMap {

	public static void main(String[] args) {
		//Scanner Class
				Scanner scan = new Scanner(System.in);
				
				//Sorted TreeMap 
				SortedMap<Integer,String> oldMap = new TreeMap<Integer,String>();
				
				//Adding Values to the TreeMap
				oldMap.put(1,"Tom and Jerry");
				oldMap.put(2,"Shin Chan");
				oldMap.put(3,"Ben 10");
				oldMap.put(4,"Jackie Chan");
				oldMap.put(5,"Ninja Hattori");
				oldMap.put(6,"Teen Titans");
				oldMap.put(7,"Kick Buttowski");
				oldMap.put(8,"Kit vs kat");
				oldMap.put(9,"Power Rangers SPD");
				oldMap.put(10,"Power Rangers Majestic Force");
				
				//Getting User Values to Access items
				System.out.println("Enter the Range of the Map to access!!");
				int startVal = scan.nextInt();
				int endVal = scan.nextInt();
				//Accessing the values of the map using subMap.
				System.out.println(oldMap.subMap(startVal, endVal).values());
	}

}
