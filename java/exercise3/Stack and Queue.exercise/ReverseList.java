/*
Requirement:
    Reverse List Using Stack with minimum 7 elements in list.
    
Entity:
   ReverseList
   
Function Declaration:
   public static void main(String[] args)
   
Jobs To Be Done:1.consider the program
                2.ArrayList is created name list
                3.the elements to the list are added 
                4.Then the list is printed
                5.stack is created named stack
                6.the elements of the list are pushed into the stack
                7.ArrayList is created named list1
                8.The elements of the stack are popped and added to the list1
                9.so the reverse list will be stored in list1
                10.the reversed list is printed
 
 */


package com.java.training.core.stack;
import java.util.ArrayList;
import java.util.Stack;


public class ReverseList {

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		for(int number = 1 ;number < 8 ;number++) {
			list.add(number);
		}
		list.forEach((number) -> System.out.print(number + " "));
		
		Stack<Integer> stack = new Stack<Integer>();
		for(int number = 0 ;number < 7 ;number++) {
			stack.push(list.get(number));
		}
		System.out.println();
		
		ArrayList<Integer> list1 = new ArrayList<Integer>();
		for(int number = 0 ;number < 7 ;number++) {
			list1.add(stack.pop());
		}
		System.out.println("Reversed list");
        list1.forEach((number) -> System.out.print(number + " "));

	}

}
