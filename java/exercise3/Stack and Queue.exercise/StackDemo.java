/*
Requirement: Create a stack using generic type and implement
                 -> Push atleast 5 elements
                 -> Pop the peek element
                 -> search a element in stack and print index value
                 -> print the size of stack
                 -> print the elements using Stream
                   
Entity:
   StackDemo
   
Function Declaration:
    public static void main(String[] args)
    
Jobs To Be Done:1.consider the program
                2.A stack is created named stack
                3.elements are pushed into the stack
                4.The peek is popped using pop
                5.index of the particular element is searched and printed using search
                6.the size of the stack is printed using size
                7.elements of the stacks are printed usin stream
 */

package com.java.training.core.stack;
import java.util.Stack;
import java.util.stream.Stream;


public class StackDemo {

	public static void main(String[] args) {
Stack<Integer> stack = new Stack<Integer>();
		
		//elements pushed into the stack
		for(int number = 0; number < 5; number++) { 
            stack.push(number); 
        }
		
		//pop the peek element
	    System.out.println("pop operation:"+stack.pop());
	   
	    //print index value of the element
	    System.out.println("index of element 2:"+stack.search(2));
	   
	    //size of the stack
	    System.out.println("Lenght of the stack:"+stack.size());
	   
	    //printing elements using stream
	    System.out.println("Printing elements using stream:");
        Stream<Integer> stream = stack.stream();
	    stream.forEach((value) -> System.out.println(value));
		

	}

}
