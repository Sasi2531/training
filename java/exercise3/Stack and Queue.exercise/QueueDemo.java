/*
Requirement:
    Create a queue using generic type and in both implementation Priority Queue,Linked list and complete following  
    1. add atleast 5 elements
    2. remove the front element
    3. search a element in stack using contains key word and print boolean value value
    4. print the size of stack
    5. print the elements using Stream 

Entity
    QueueDemo

Function Declaration:
    public static void main(String[] args)
Jobs to be done:
    1. Create a package com.java.training.stack.
      1.1 Declare the class QueueDemo as public.
    2. import the Queue, LinkedList, PriorityQueue and Stream packages.
    3. Create an object string for the Queue class of String type and add elements.
    4.Print the following
      4.1 print the list elements,removed element,updated list element.
      4.2create a value of type boolean   
      4.3 Print the size of the queue 
      4.4print the elements using Stream.
    5. Use the same procedure for the PriorityQueue and print the results.
pseudo code:
    public class QueueDemo {

	public static void main(String[] args) {
		Queue<String> string = new LinkedList<>();
		//add elements to the list
		   string.add("KTM");
           string.add("R15");
           string.add("RE");
           string.add("XSR155");
           string.offer("FZv3");
		// searching using contains keyword
          boolean value = string.contains("RE");
		  Stream<String> stream = string.stream();
		  stream.forEach(iteration -> System.out.print(iteration + " "));
		Queue<Integer> integer = new PriorityQueue<>();
		// searching using contains keyword
          boolean value1 = string.contains("NS220");
		  Stream<Integer> stream1 = integer.stream();
		  stream1.forEach(iteration1 -> System.out.print( iteration1 + " "));
*/


package com.java.training.core.stack;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Stream;


public class QueueDemo {

	public static void main(String[] args) {
		
			Queue<String> string = new LinkedList<>();
	        string.add("KTM");
	        string.add("R15");
	        string.add("RE");
	        string.add("XSR155");
	        string.offer("FZv3");
	        System.out.println("The list elements are" + string);
	        System.out.println("The removerd element is" + string.remove());
	        System.out.println("the queue after removal of first element is" + string);
	        // searching using contains keyword
	        boolean value = string.contains("RE");
	        System.out.println("the boolean value is " + value);
	        // To print the size of the stack
	        System.out.println("the size of the queue is" + string.size());
	        
	        Stream<String> stream = string.stream();
	        System.out.println("The queue elements printed using Stream ");
	        stream.forEach(iteration -> System.out.print(iteration + " "));
	       
	        Queue<Integer> integer = new PriorityQueue<>();
	        integer.add(10);
	        integer.add(20);
	        integer.add(30);
	        System.out.println("The list elements are" + integer);
	        System.out.println("The removerd element is" + integer.remove());
	        System.out.println("the queue after removal of first element is" + integer);
	        // searching using contains keyword
	        boolean value1 = string.contains("NS220");
	        System.out.println("the boolean value is " + value1);
	        // To print the size of the stack
	        System.out.println("the size of the queue is" + integer.size());
	        
	        Stream<Integer> stream1 = integer.stream();
	        System.out.print("The queue elements printed using Stream ");
	        stream1.forEach(iteration1 -> System.out.print( iteration1 + " "));

	}

}
