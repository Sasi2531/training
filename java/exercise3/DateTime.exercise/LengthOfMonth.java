/*
Requirement:
    To Write an example that, for a given year, reports the length of each month within that particular year.
    
Entity:
    LengthOfMonth 

Function Declaration:
    public static void main(String[] args);
    maxLength();

Jobs To be Done;
    1.Get the year from user
    2.invoke the class Year and pass the user input year as parameter
    3.Get the month from user
    4.invoke the Month  and pass the user input month as parameter 
    5.invoke max length method and find the length of the month
    6.Print the result.

pseudo code:

    public class LengthOfMonth {
    
        public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int year = scanner.nextInt();
        Year thisYear = Year.of(year);
        int month = scanner.nextInt();
        Month thismonth = Month.of(month);
        System.out.println(thismonth.maxLength());
    }
}

 */

package com.java.training.coreDateTime;
import java.time.Month;
import java.time.Year;
import java.util.Scanner;


public class LengthOfMonth {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the year \n");
        int year = scanner.nextInt();
        Year thisYear = Year.of(year);
        System.out.print("Enter the month \n");
        int month = scanner.nextInt();
        Month thismonth = Month.of(month);
        System.out.println(thismonth.maxLength());
        scanner.close();
	}

}
