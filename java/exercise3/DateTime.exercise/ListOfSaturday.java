/*
Requirement :
    To Write an example that, for a given month of the current year, lists all of the Saturdays in that month.

Entity:
    ListOfSaturday

Function Declaration:
    public static void main(String[] args)
    firstInMonth();

Jobs to be done :
    1.Get the month from user
    2.invoke the Month class and pass the user input as parameter
    3.invoke the LocalDate class and pass the parameter and store it in its object 
    4.With that object get the Saturday's date and print it  
    5.Use the loop and check the month
      and print the next saturday's date
      
Pseudo Code:

public class ListOfSaturday {
        
    public static void main(String[] args) {
        Scanner scanner//use scanner for input month
        Month month1 = Month.valueOf(month.toUpperCase());
        System.out.print( month1);
        LocalDate date = Year.now().atMonth(month1).atDay(1)
                         .with(TemporalAdjusters.firstInMonth(DayOfWeek.SATURDAY));
        Month newMonth = date.getMonth();
        while (newMonth == month1) {
            System.out.printf("%s%n", date);
            date = date.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
            newMonth = date.getMonth();
        }
    }
}
 */

package com.java.training.coreDateTime;
import java.time.Month;
import java.time.Year;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.Scanner;

public class ListOfSaturday {

	public static void main(String[] args) {
		 Scanner scanner = new Scanner(System.in);
	        System.out.printf("Enter the month to find the Satudays %n");
	        String month = scanner.next();
	        Month month1 = Month.valueOf(month.toUpperCase());
	        System.out.printf("For the month of ", month1);
	        LocalDate date = Year.now().atMonth(month1).atDay(1)
	                         .with(TemporalAdjusters.firstInMonth(DayOfWeek.SATURDAY));
	        Month newMonth = date.getMonth();
	        while (newMonth == month1) {
	            System.out.printf("%s%n", date);
	            date = date.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
	            newMonth = date.getMonth();
	        }
	        scanner.close();

	}

}
