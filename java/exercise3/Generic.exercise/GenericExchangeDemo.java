/*
Requirement :
    Write a generic method to exchange the positions of two different elements in an array.

Entity:
    GenericExchangeDemo

Function Declaration:
    public static <T> void swap(T[] list, int firstNumber, int lastNumber)
    public static <T> void printSwap(List<T> list1, int firstNumber, int secondNumber)
    public static void main(String[] args)

Jobs to be done:
    
    1. Create a method swap() of type T and swap the elements using temporary
       variables.
    2. Create another method Swap() and swap by using Collections.swap().
    3. Under a main method create an array and call the swap method.
    4. print the result.
    5. Create an object for List of type Integer and call Swap() method
    6. Print the result.
*/

package com.java.training.core.generic;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GenericExchangeDemo {
	public static <T> void swap(T[] number, int number1, int number2) {
        T temporary = number[number1];
        number[number1] = number[number2];
        number[number2] = temporary
;
    }

	public static void main(String[] args) {
		Integer[] list = {110, 250, 300, 234, 540, 600};
        swap(list, 2, 4);
        
        List<Integer> list1 = new ArrayList<>(Arrays.asList(list));
        Swap(list1, 2, 4);
        System.out.println(list1);
    }


    private static void Swap(List<Integer> list1, int i, int j) {
        
		
		

	}

}
