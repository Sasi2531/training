package com.java.training.core.generic;

import java.util.*;
class  OddPredicate implements UnaryPredicate<Integer> {
    public boolean Demo (Integer number) {
        return number % 2 != 0;
    }
}

public class GenericCountDemo {
	public static void main(String[] args) {
		Collection<Integer> list = new Arrays.asList(1,2,3,4);
        int count = Algorithm.countIf(list, new OddPredicate());
        System.out.println("Number of odd integers = " + count);


	}

}
