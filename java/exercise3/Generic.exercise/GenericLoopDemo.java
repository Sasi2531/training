/*
Requirement :
    Write a program to demonstrate generic - for loop, for list, set and map

Entity:
    GenericLoopDemo
 
Function Declaration:
    add();
    getKey();
    setKey();
    public static void main(String[] args );
    
Jobs To be Done :
    1.Add the values to list and set
    2.put the keys and values to map
    3.print list set and map using for loop 

 */


package com.java.training.core.generic;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GenericLoopDemo {
	 public static void main(String[] args) {
	        
	        //set using for each loop
	        Set<String> set = new HashSet<>();
	        set.add("money");
	        set.add("honey");
	        set.add("funny");
	        for (String words : set) {
	            System.out.println("the words are " + words);
	        }
	        
	        //list using for each loop
	        List<Integer> list = new ArrayList<>();
	        list.add(1);
	        list.add(2);
	        list.add(3);
	        list.add(4);
	        for (Integer number : list) {
	            System.out.println("the numbers are " + number);
	        }
	        
	        //map using for each loop
	        HashMap<String , String> map = new HashMap<>();
	        map.put("111"," AAA");
	        map.put("222","BBB");
	        map.put("333", "CCC");
	        map.put("444","DDD");
	        for (String i : map.keySet()) {
	            System.out.println("key: " + i + " value: " + map.get(i));
	        }
	    }

	}

