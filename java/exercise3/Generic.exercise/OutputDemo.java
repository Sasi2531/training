package com.java.training.core.generic;

public class OutputDemo {

	public static void main(String[] args) {
		MyGen<Integer> m = new MyGen<Integer>();  
        //m.set("merit");    // compile time error
        System.out.println(m.get());
    }
}
class MyGen<T> {

    T var;

    void  set(T var) {
        this.var = var;
    }
    T get() {
        return var;
		

	}

}
