package com.java.training.core.generic;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

class Employee {

    private String name;
   

    public Employee(String name) {
        this.name = name;
        
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
    

class EmployeeList implements Iterable<Employee> {
    private List<Employee> employees;
    
    public EmployeeList() {
        employees = new ArrayList<Employee>();
    }
    
   
    
    public void addEmployee(Employee employee) {
        employees.add(employee);
    }
    
    public void removeEmployee(Employee employee) {
        employees.remove(employee);
    }



    @Override
    public Iterator<Employee> iterator() {
        // TODO Auto-generated method stub
        return null;
    }
    
    
}


public class IterableInterfaceDemo {
	public static void main(String[] args) {
        Employee employee1 = new Employee("John");
        Employee employee2 = new Employee("Joseph");
        EmployeeList employeeList = new EmployeeList();
        employeeList.addEmployee(employee1);
        employeeList.addEmployee(employee2);
        for (Employee employee : employeeList) {
            System.out.println(employee);
        }
    }

}
