/*
Requirements:
Use the Java API documentation for the Box class (in the javax.swing package) to help you answer the following questions.
    - What static nested class does Box define?
    - What inner class does Box define?
    - What is the superclass of Box's inner class?
    - Which of Box's nested classes can you use from any class?
    - How do you create an instance of Box's Filler class?
	
Entity:
  None
  
Function Declaration:
  None
*/

Answer:
a.Box.Filler
b.Box.AccessibleBox
c.[java.awt.]Container.AccessibleAWTContainer
d.Box.Filler
e.new Box.Filler(minDimension, prefDimension, maxDimension)
