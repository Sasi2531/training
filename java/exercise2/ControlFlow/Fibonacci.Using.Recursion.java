print fibinocci using recursion.
/*
Requirement:
    To find fibonacci series using Recursion.

Entity:
    Here the entity FibonacciUsingRecursion has been given.

Function Declaration:
    public static void printFibonacci, is the function declared.

Jobs to be Done:
    1. Declare the class FibonacciUsingRecursion.
    2. Declare and assign the first two variable as 1 and 0 and declare the third variable sum.
    3. Now declare the function as public static void printFibonacci(int i) and give the body of
       the function.
    4. Call the function printFibonacci(int i) by declaring the variable n.
    5. Now print the series using print statement.
*/

import java.util.Scanner;
public class FibonacciUsingRecursion {
        public static int a = 0;
        public static int b = 1;
        public static int sum;
        public static void printFibonacci(int i) {
            if(i > 0) {
                sum = a + b;
                a = b;
                b = sum;
                System.out.print(a + " ");
                printFibonacci(i - 1);
            }
        }
    public static void main(String[] args) {
        int n;
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        printFibonacci(n);
    }
}