/*
Requirement:
    To identify what is wrong in the given interface program.
    public interface SomethingIsWrong {
        void aMethod(int aValue){
            System.out.println("Hi Mom");
        }
    }

Entity:
    SomethingIsWrong is the Entity is used here.

Function Declaration:
    void aMethod(int aValue) is the function declared.

Jobs to be done:
    1. Look at the following program and identify the error.
    2. After identifing the error Correct the program.
*/

Answer:
Only default and static variables can implements can have a method implementations
so the correct format to declare the method is shown below.

public interface SomethingIsWrong {
    default void aMethod(int aValue){     // Here default or static can be used.
        System.out.println("Hi Mom");
    }
}