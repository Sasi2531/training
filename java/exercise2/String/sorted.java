/*Requirements:
  sort and print following String[] alphabetically ignoring case. Also convert and print even indexed Strings into uppercase
       { "Madurai"
	   , "Thanjavur"
	   , "TRICHY"
	   , "Karur"
	   , "Erode"
	   , "trichy"
	   , "Salem" }
	   
Entites: 
  Sort is class is used in this program.
  
Function Declaration:
  There is no function declared in the given program.
    
JobsToBeDone:
   1.Create a public class sort.
   2.Inside main function,give vales to string.
   3.sort the given array.
   4.print the sorted array.
   5.using for loop,and if condition,chance the string values to uppercase.
*/

SOLUTION:
import java.util.Arrays; 
public class Sort{
    public static void main(String[] args){
        String [] place = {"Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" };
        Arrays.sort(place);
        System.out.println(Arrays.toString(place));
        for(int i=0;i<place.length;i++)
        {
            if(i%2==0) {
                place[i] = place[i].toUpperCase();
            }
        }
        System.out.print(Arrays.toString(place));
    }
}