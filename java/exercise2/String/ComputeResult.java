/*Requirement:
  To find the value of result after each numbered line executes of the following program
  public class ComputeResult {
        public static void main(String[] args) {
            String original = "software";
            StringBuilder result = new StringBuilder("hi");
            int index = original.indexOf('a');

    /1/   result.setCharAt(0, original.charAt(0));
    /2/   result.setCharAt(1, original.charAt(original.length()-1));
    /3/   result.insert(1, original.charAt(4));
    /4/   result.append(original.substring(1,4));
    /5/   result.insert(3, (original.substring(index, index+2) + " "));

            System.out.println(result);
        }
    }

Entities:
  ComputeResult is the class used in this program.

Function Declaration:
  charAt(), length(), substring(), setCharAt(), insert(), indexOf(), append().
  
Jobs to be done:
    1) Creating the class ComputeResult
    2) Declaring the string variable original as software.
    3) Creating the object as result assigning hi  to it.
    4) Declaring the variable index of type int and assiging the index of 'a' in the string original
    4) First line sets the first character of the string original 'h' to 's'.
    5) Second,i is removed and e is set as 'i' is the character in the final result value.
    6) Third, w is inserted at first index.
    7) Forth, oft is appended to the result.
    8) Last, ar is added in second index and a space is added and the remaining character gets
       printed
*/

Solution:

si
se
swe
sweoft
swear oft