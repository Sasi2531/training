/*
Requirement:
       Write a program that computes your initials from your full name and displays them.
	   
Entities:
    ComputeInitials
	
Function Declaration:
          charAt() 
          length()
		  
Jobs to be done:          
         1.Declare the class ComputeInitials.
         2.Declaring the string variable as myName.
         3.Then use for loop,Assign the value to the new variable i.
         4.continue the process till the condition meets i > length.
         5.using the uppercase in the if condition.
         6.Now print the result.
*/ 
SOLUTION:      
public class ComputeInitials {
    public static void main(String[] args) {
        String myName = "R.Sasi Rekha";
        StringBuffer myInitials = new StringBuffer();
        int length = myName.length();

        for (int i = 0; i < length; i++) {
            if (Character.isUpperCase(myName.charAt(i))) {
                myInitials.append(myName.charAt(i));
            }
        }
        System.out.println("My initials are: " + myInitials);
    }
}