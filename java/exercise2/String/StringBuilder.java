/*Requirement:
    To find What is the initial capacity of the following string builder?
    StringBuilder sb = new StringBuilder("Able was I ere I saw Elba.");

Entity:
    There is no entity is defined.
    
Function declaration:
    There is no function is declared.
    
Jobs to be done:
    1)Find the length of teh string and add 16 in it.
    It is the initial capacity of the string
*/
SOLUTION:
    Length of the given String = 26
    Initial capacity of the string = 26 + 16 = 42.