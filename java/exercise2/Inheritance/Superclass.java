/*
Requirement:
Consider the following two classes:
    public class ClassA {
        public void methodOne(int i) {
        }
        public void methodTwo(int i) {
        }
        public static void methodThree(int i) {
        }
        public static void methodFour(int i) {
        }
    }

    public class ClassB extends ClassA {
        public static void methodOne(int i) {
        }
        public void methodTwo(int i) {
        }
        public void methodThree(int i) {
        }
        public static void methodFour(int i) {
        }
    }
    a. Which method overrides a method in the superclass?
    b. Which method hides a method in the superclass?
    c. What do the other methods do?

Entities:
   ClassA
   ClassB
 
Function Declaration:
  public  void methodOne(int i) 
  public void methodTwo(int i) 
  
Jobs To be done:
  1.consider the given program.
  2.Then,Which method overrides a method in the superclass.
  3.Then find which method hides a method in the superclass.
  4.Then, what do the other methods do.
*/
  
OUTPUT:
a.methodTwo.
b.methodFour.
c.They cause compile-time errors.
        