/*
Requirement:
    To demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of
    objects

Entities:
    Cat.

Function Declaration:
    public void sound().

Jobs to be done:
    1. Declare the class Cat.
    2. call the method sound().
    3. Print the the statement as given.
*/

public class Cat extends Animal {
    public void sound() {
        System.out.println("the cat meows");
    }
}