public class NumberHolder {
    public int anInt;
    public float aFloat;
    public static void main(String[] args) {
    NumberHolder aNumberHolder = new NumberHolder();
    aNumberHolder.anInt = 5;
    aNumberHolder.aFloat = 7.4f;
    System.out.println(aNumberHolder.anInt);   //5
    System.out.println(aNumberHolder.aFloat);   //7.4
    }
}