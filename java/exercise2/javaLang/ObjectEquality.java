/*
Requirement:
    To demonstrate object equality using Object.equals() vs ==, using String objects.

Entity:
    ObjectEquality

Function Declaration:
    No Function.

Jobs To be Done:
    1) Created the class ObjectEquality.
    2) Declaring the objects s1 and s2 as value HELLO
    3) For ' == ' operator it returns false because it doesn't have same address.
    4) For Equals() function, it returns true.
*/
SOLUTION:
public class ObjectEquality { 

    public static void main(String[] args) 
    {
        String s1 = new String("HELLO"); 
        String s2 = new String("HELLO"); 
        System.out.println(s1 == s2); 
        System.out.println(s1.equals(s2)); 
    } 
}