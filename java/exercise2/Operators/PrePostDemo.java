In the following program, explain why the value "6" is printed twice in a row:

/*
Requirement:
  The following program, explain  the value "6" is printed twice in a row.
  class PrePostDemo {
           public static void main(String[] args){
               int i = 3;
               i++;
               System.out.println(i);    // "4"
               ++i;
               System.out.println(i);    // "5"
               System.out.println(++i);  // "6"
               System.out.println(i++);  // "6"
               System.out.println(i);    // "7"
           }
       }

  
Entities:
  ArithmeticDemo
  
Function Declaration:
  No function declaration
  
Jobs to be done:
  In the given program the value "6" is printed twice in a row.
*/
OUTPUT:
The code System.out.println(++i); evaluates to 6, because the prefix version of ++ evaluates to the incremented value. 
The next line, System.out.println(i++); evaluates to the current value (6), then increments by one. 
So "7" doesn't get printed until the next line.