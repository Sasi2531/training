/*
Requirement:
    To demonstrate overloading with Wrapper types.

Entity:
    WrapperOverLoading

Function declaration:
    check(Integer),check(Float),check(Double),check(Long)
    public static void main(String[] args)

Jobs to be done:
    1.Create a class WrapperOverLoading.
    2.Declare the methods as check with different wrapper types.
    3.Create an object for the class and pointer as value.
    4.Call the methods with different data type values.
    5.Print the respective values to wrapper types.
*/

SOLUTION:
public class WrapperOverLoading {

    public void check(Integer number) {
        System.out.println("Integer : " + number);
    }
    public void check(Float number) {
        System.out.println("Float : " + number);
    }
    public void check(Double number) {
        System.out.println("Double : " + number);
    }
    public void check(Long number) {
        System.out.println("Long : " + number);
    }
    public static void main(String[] args) {
        WrapperOverLoading value = new WrapperOverLoading();
        value.check(new Integer(50));
        value.check(new Float(10.5f));
        value.check(new Double(6.57839d));
        value.check(new Long(567890855089L));
    }
}