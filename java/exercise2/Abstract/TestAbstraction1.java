/*Requirement:
 To Demonstrate abstract classes using shape class.
 
Entities:
  abstracte class shape is used in this program.
  Classes Rectangle,Circle,TestAbstraction1 are used.
  
Function Declaration:
   Draw function is declared is this program.
   
Job to be Done:
 1.Create an abstract class shape and declare a function named draw.
 2.Create a class Rectangle and inherit shape.
 3.Create a class Circle1 and inherit shape.
 4.Create a main class TestAbstraction1 and create an object for shape.
*/
Solution:
abstract class Shape{  
abstract void draw();  
}  

class Rectangle extends Shape{  
void draw(){System.out.println("drawing rectangle");}  
}  
class Circle1 extends Shape{  
void draw(){System.out.println("drawing circle");}  
}  

class TestAbstraction1{  
public static void main(String args[]){  
Shape s=new Circle1();
s.draw();  
}  
}  