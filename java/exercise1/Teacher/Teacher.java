public class Teacher{ 
 
    public String teacherId; 
    private String teacherName;
    protected String dateOfJoining;
    public Teacher(String teacId,String teacName,String teacdoj) { 
        teacherId = teacId;
        teacherName = teacName;
        dateOfJoining = teacdoj;
    }
    public void printInfo() {  
        System.out.println("TeacherName: " + teacherName);
        TeacherDept teacdept = new TeacherDept("Chemistry");
        teacdept.display(); 
    }
    class TeacherDept{  
        private String teacherDept;
        
        public TeacherDept(String teacDept) {
            teacherDept = teacDept;
        }
        public void display() {
            System.out.println("TeacDept: " + teacherDept);
        }
    }
}
class Main{
    public static void main(String[] args) {
        Teacher teac = new Teacher("101","Sasirekha","25-03-2001");
        teac.printInfo(); 
        System.out.println("TeacherId: " + teac.teacherId); 
        System.out.println("Teacherdoj: " + teac.dateOfJoining);
    }
}