import com.college.training.core.Staff;

public class StaffDetail {
    
    public static void main(String[] args) {
        StaffDetail.staffDetail = new StaffDetail();
        System.out.println("staffName=" + StaffDetail.staffName);//error because (private Access Modifier)The code is only accessible within the declared class
        System.out.println("staffAge=" + StaffDetail.staffAge);//error because (private Access Modifier)The code is only accessible within the declared class
        System.out.println("EmployeeMail=" + StaffDetail.staffMailId);//error because (private Access Modifier)The code is only accessible within the declared class
	}
}