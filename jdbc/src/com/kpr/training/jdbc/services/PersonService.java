/*
Requirement:
	To perform the CRUD operation of the Person.
 
Entity:
	1.Person
    2.PersonService
    3.AppException
    4.ErrorCode
 
Function declaration:
	public static void create(Person person, Address address)
	public static void read(long id, boolean addressFlag)
	public static void readAll()
	public static void update(long id, Person person, Address address)
	public static void delete(long id)

Jobs To Be Done:
    1. Create a Person.
    2. Read a record in the Person.
    3. Read all the record in the Person.
    4. Update a Person.
    5. Delete a Person.
*/


package com.kpr.training.jdbc.services;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.sql.Timestamp;
import com.kpr.training.jdbc.constants.QueryStatements;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;

public class PersonService {
	public static long create(Person person, Address address) {
		long noOfRowsAffected = 0;
		long personID = 0;
		if (address.getPostalCode() == 0) {
			throw new AppException(ErrorCode.POSTAL_CODE_ZERO);
		} else {
			LocalDateTime d = LocalDateTime.now();
			Timestamp date = Timestamp.valueOf(d);
			JdbcConnection jc = new JdbcConnection();
			jc.createConnection();
			long id = AddressService.create(address);

			try {
				jc.ps = jc.con.prepareStatement(QueryStatements.createPersonQuery,
						jc.ps.RETURN_GENERATED_KEYS);
				jc.ps.setString(1, person.getName());
				jc.ps.setString(2, person.getEmail());
				jc.ps.setDate(3, person.getBirthDate());
				jc.ps.setTimestamp(4, date);
				jc.ps.setLong(5, id);
				noOfRowsAffected = jc.ps.executeUpdate();
				ResultSet personId = jc.ps.getGeneratedKeys();
				while (personId.next()) {
					personID = personId.getLong(1);
				}
				if (noOfRowsAffected == 0) {
					throw new AppException(ErrorCode.PERSON_CREATION_FAILS);
				}

			} catch (SQLException e) {
				AddressService.delete(id);
				throw new AppException(ErrorCode.SQLException);
			}
			jc.closeConnection();
		}
		return personID;
	}

	public static void read(long id, boolean addressFlag) {
		long personID = 0;
		String name = null;
		String email = null;
		long addressID = 0;
		Date birthDate = null;
		Timestamp createdDate = null;

		if (id == 0) {
			throw new AppException(ErrorCode.ID_ZERO);
		}

		JdbcConnection jc = new JdbcConnection();
		jc.createConnection();

		try {
			jc.ps = jc.con.prepareStatement(QueryStatements.readPersonQuery);
			jc.ps.setLong(1, id);
			ResultSet result = jc.ps.executeQuery();

			while (result.next()) {
				personID = result.getLong("id");
				name = result.getString("name");
				email = result.getString("email");
				addressID = result.getLong("address_id");
				birthDate = result.getDate("birth_date");
				createdDate = result.getTimestamp("created_date");
			}

			if (email == null) {
				throw new AppException(ErrorCode.PERSON_ID_NOT_FOUND);
			} else {
				System.out.println(
						personID + " " + name + " " + email + " " + birthDate + " " + createdDate);
				if (addressFlag == true) {
					AddressService.read(addressID);
				}
			}

		} catch (SQLException e) {
			throw new AppException(ErrorCode.SQLException);
		}
	}

	public static void readAll() {
		ArrayList<Person> persons = new ArrayList<>();
		ArrayList<Long> personIDs = new ArrayList<>();
		ArrayList<Long> addressIDs = new ArrayList<>();
		ArrayList<Timestamp> createdDate = new ArrayList<>();
		JdbcConnection jc = new JdbcConnection();
		jc.createConnection();

		try {
			jc.ps = jc.con.prepareStatement(QueryStatements.readAllPersonQuery);
			ResultSet result = jc.ps.executeQuery();
			while (result.next()) {
				Person person = new Person(result.getString("name"), result.getString("email"),
						result.getDate("birth_date"));
				persons.add(person);
				personIDs.add(result.getLong("id"));
				addressIDs.add(result.getLong("address_id"));
				createdDate.add(result.getTimestamp("created_date"));
			}
		} catch (SQLException exception) {
			throw new AppException(ErrorCode.SQLException);
		}
		for (int element = 0; element < personIDs.size(); element++) {
			System.out.println(personIDs.get(element) + " " + persons.get(element).getName() + " "
					+ persons.get(element).getEmail() + " " + persons.get(element).getBirthDate()
					+ " " + createdDate.get(element));
			AddressService.read(addressIDs.get(element));
		}

		jc.closeConnection();
	}

	public static void update(long id, Person person, Address address) {

		int numberOfRowsAffected = 0;
		if (address.getPostalCode() == 0) {
			throw new AppException(ErrorCode.POSTAL_CODE_ZERO);

		} else if (id == 0) {
			throw new AppException(ErrorCode.ID_ZERO);
		} else {
			JdbcConnection jc = new JdbcConnection();
			jc.createConnection();

			try {
				jc.ps = jc.con.prepareStatement(QueryStatements.updatePersonQuery);
				jc.ps.setString(1, address.getStreet());
				jc.ps.setString(2, address.getCity());
				jc.ps.setInt(3, address.getPostalCode());
				jc.ps.setString(4, person.getName());
				jc.ps.setString(5, person.getEmail());
				jc.ps.setDate(6, person.getBirthDate());
				jc.ps.setLong(7, id);
				numberOfRowsAffected = jc.ps.executeUpdate();
			} catch (SQLException exception) {
				throw new AppException(ErrorCode.SQLException);
			}
			if (numberOfRowsAffected == 0) {
				throw new AppException(ErrorCode.PERSON_UPDATION_FAILS);
			} else {
				System.out.println("Updation Successful");
				jc.closeConnection();
			}
		}
	}

	public static void delete(long id) {
		if (id == 0) {
			throw new AppException(ErrorCode.ID_ZERO);
		} else {
			int numberOfRowsAffected = 0;
			JdbcConnection jc = new JdbcConnection();
			jc.createConnection();
			try {
				jc.ps = jc.con.prepareStatement(QueryStatements.deletePersonQuery);
				jc.ps.setLong(1, id);
				numberOfRowsAffected = jc.ps.executeUpdate();
			} catch (SQLException exception) {
				throw new AppException(ErrorCode.SQLException);
			}
			if (numberOfRowsAffected == 0) {
				throw new AppException(ErrorCode.PERSON_DELETION_FAILS);
			} else {
				System.out.println("Deletion Successful");
				jc.closeConnection();
			}
		}
	}

}
