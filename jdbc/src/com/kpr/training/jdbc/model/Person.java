/*
Requirement:
 		To create a POJO class for person.

Entity:
 		Person
 
Method Signature:
 		public Person(String name, String email, Date birthDate, Calendar createdDate)
 		public String getName()
 		public void setName(String name)
  		public String getEmail()
  		public void setEmail(String email)
  		public Date getBirthDate()
  		public void setBirthDate(Date birthDate)
  	
Jobs to be Done:
  		1)Declare the fields as private.
  		2) Create a constructor with four parameters which is the values of fields.
 			2.1)Increment the id value and assign the value to id.
  			2.2)Assign the values of fields with argument value.
  			2.3)Increment the created date of the sql file by 1.
  		3) Create the method to get the name. 
  		4) create the method to get the email.
  		5) Create the method to get the birthDate.
  		6) Create the method to assign the value of name.
  		7) create the method to assign the value of email.
  		8) Create the method to assign the value of city.
  		9) Create the method to assign the value of birthDate.
  
pseudo Code:
  	class Person {
  		private long id;
		private String name;
		private String email;
		private Date birthDate;
		private LocalDate createdDate;
		private long addressId;
		
		
		public Person(String name, String email, Date birthDate, Calendar createdDate) {
			this.name = name;
			this.email = email;
			this.birthDate = birthDate; 
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public Date getBirthDate() {
			return birthDate;
		}
		public void setBirthDate(Date birthDate) {
			this.birthDate = birthDate;
		}
  	}
  
 */


package com.kpr.training.jdbc.model;
import java.sql.Timestamp;
import java.sql.Date;
public class Person {
	private long id;
	private String name;
	private String email;
	private Date birthDate;
	private Timestamp created_date;
	private long address_id;


	public Person(String name, String email, Date birthDate) {
		this.name = name;
		this.email = email;
		this.birthDate = birthDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Date getBirthDate() {
		return birthDate;
	}

}
