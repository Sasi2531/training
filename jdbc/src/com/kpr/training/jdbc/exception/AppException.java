/*
 * Requirement:
 * 		To write the AppException for the person and address service
 * 
 * Entity:
 * 		AppException
 * 
 * Method Signature:
 * 		public AppException(ExceptionCode code)
 * 
 * Jobs To Be Done:
 * 		1) The respective errors using its code number are declared in enum.
 * 		2) The constructor is created with reference to error thrown from address and person service.
 * 		3) The respective error message is printed.
 * 
 * Pseudo code:
 * 
 *  enum ExceptionCode {
 *  	//create the respective code and its message.
 *  	TYPE_OF_ERRORCODE(501, "Error Message");
 *  }
 *  
 *  class AppException {
 *  	private int errorCode;
 *  	private String errorMsg;
 *  
 *  	public AppException(ExceptionCode code) {
 *  		System.out.println("Error Code" + AppException.errorCode + AppException.errorMsg);
 *  	}
 *  }
 *  
 */

package com.kpr.training.jdbc.exception;

public class AppException extends RuntimeException {
	private int errorCode;
	private String errorMsg;

	public AppException(ErrorCode code) {
		this.errorMsg = code.getMsg();
		this.errorCode = code.getId();
		System.out.println(
				"Error Code " + AppException.this.errorCode + " " + AppException.this.errorMsg);
	}

}
