package com.kpr.training.jdbc.exception;

public enum ErrorCode {
	POSTAL_CODE_ZERO(401, "postal code should not be zero"), SQLException(402,
			"SQL exception occurs"), ADDRESS_CREATION_FAILS(403,
					"Address was not created"), ID_ZERO(404,
							"ID should not be zero"), ADDRESS_UPDATION_FAILS(405,
									"Failed to update Address"), ADDRESS_DELETION_FAILS(406,
											"Failed to delete Address"), ID_NOT_FOUND(407,
													"No Address is found with this id"), PERSON_ID_NOT_FOUND(
															408,
															"No person found in this id"), PERSON_CREATION_FAILS(
																	409,
																	"Person was not created"), PERSON_UPDATION_FAILS(
																			410,
																			"Failed to update Person"), PERSON_DELETION_FAILS(
																					411,
																					"Failed to delete Person"), IO_EXCEPTION(
																							412,
																							"IO exception occurs");

	private final int id;
	private final String msg;

	ErrorCode(int id, String msg) {
		this.id = id;
		this.msg = msg;
	}

	public int getId() {
		return this.id;
	}

	public String getMsg() {
		return this.msg;
	}

}
