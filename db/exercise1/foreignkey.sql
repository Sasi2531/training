11.Read Records by Sub-queries by using two tables FOREIGN KEY relationships

CREATE TABLE `college`.`library` (
  `library_id` INT NOT NULL,
  `library_books` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`library_id`));

ALTER TABLE `college`.`student` 
ADD CONSTRAINT `library_id_library`
  FOREIGN KEY (`library_id`)
  REFERENCES `college`.`library` (`library_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

INSERT INTO `college`.`library` (`library_id`, `library_books`) VALUES ('1', 'Maths');
INSERT INTO `college`.`library` (`library_id`, `library_books`) VALUES ('2', 'English');
INSERT INTO `college`.`library` (`library_id`, `library_books`) VALUES ('3', 'Novel');
INSERT INTO `college`.`library` (`library_id`, `library_books`) VALUES ('4', 'Sciencs');
INSERT INTO `college`.`library` (`library_id`, `library_books`) VALUES ('5', 'Physics');

UPDATE `college`.`student` SET `library_id` = '1' WHERE (`student_id` = '1');
UPDATE `college`.`student` SET `library_id` = '2' WHERE (`student_id` = '2');
UPDATE `college`.`student` SET `library_id` = '3' WHERE (`student_id` = '3');
UPDATE `college`.`student` SET `library_id` = '4' WHERE (`student_id` = '4');
UPDATE `college`.`student` SET `library_id` = '1' WHERE (`student_id` = '5');
UPDATE `college`.`student` SET `library_id` = '5' WHERE (`student_id` = '6');
UPDATE `college`.`student` SET `library_id` = '2' WHERE (`student_id` = '7');

select student.student_name
       ,library.library_id 
	   from student join library on student.library_id=library.library_id;

