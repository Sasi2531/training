10.USE inbuilt Functions - now,MAX, MIN, AVG, COUNT, FIRST, LAST, SUM


ALTER TABLE `college`.`student` 
ADD COLUMN `marks` INT NOT NULL AFTER `library_id`;

UPDATE `college`.`student` SET `marks` = '90' WHERE (`student_id` = '1');
UPDATE `college`.`student` SET `marks` = '85' WHERE (`student_id` = '2');
UPDATE `college`.`student` SET `marks` = '75' WHERE (`student_id` = '3');
UPDATE `college`.`student` SET `marks` = '70' WHERE (`student_id` = '4');
UPDATE `college`.`student` SET `marks` = '95' WHERE (`student_id` = '5');
UPDATE `college`.`student` SET `marks` = '80' WHERE (`student_id` = '6');
UPDATE `college`.`student` SET `marks` = '60' WHERE (`student_id` = '7');

SELECT student_name,student_id,now() as result from student;

SELECT student_name,student_id,department_id,max(marks) as highest from student;

SELECT student_name,student_id,department_id,min(marks) as youngest from student; 

SELECT avg(marks) as average from student;

SELECT sum(marks) as total from student;

SELECT count(student_id) as student_strength from student;