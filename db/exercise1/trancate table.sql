4.Truncate Records From table

CREATE TABLE `college`.`subject` (
  `subject_id` INT NOT NULL,
  `subject_name` VARCHAR(45) NOT NULL);
  
  ALTER TABLE `college`.`subject` 
ADD PRIMARY KEY (`subject_id`);

INSERT INTO `college`.`subject` (`subject_id`, `subject_name`) VALUES ('01', 'Maths');
INSERT INTO `college`.`subject` (`subject_id`, `subject_name`) VALUES ('02', 'English');
INSERT INTO `college`.`subject` (`subject_id`, `subject_name`) VALUES ('03', 'Science');

TRUNCATE TABLE subject;