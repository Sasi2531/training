9. Show consolidated result for the following scenarios;
a) collected and uncollected semester fees amount per semester
for each college under an university. Result should be filtered
based on given year.
b) Collected semester fees amount for each university for the given
year


a)SELECT university.`university_name`
      ,college.`name`
      ,semester_fee.`semester`
      ,SUM(amount) AS 'collected_fees' 
      ,semester_fee.paid_year
FROM semester_fee
    ,university
    ,student
    ,college
WHERE semester_fee.stud_id = student.id
 AND student.college_id = college.id
 AND college.univ_code = university.univ_code
 AND university_name = 'anna'
 AND paid_year = '2022'
 AND semester = '2'
 AND paid_status = 'notpaided'; 
 
 b)SELECT university.`university_name`
      ,SUM(amount) AS 'collected_fees' 
      ,semester_fee.paid_year
FROM semester_fee
    ,university
    ,student
    ,college
WHERE semester_fee.stud_id = student.id
 AND student.college_id = college.id
 AND college.univ_code = university.univ_code
 AND university.university_name = 'NIT'
 AND paid_status = 'paided'
 AND paid_year = '2018'

