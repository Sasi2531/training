9.USE Where to filter records using AND,OR,NOT,LIKE,IN,ANY, wildcards ( % _ )

SELECT student_name
      ,student_id
	   FROM student
	   Where department_id = "18ec05" and student_id="5";
       
SELECT student_name
      ,student_id
	   FROM student
	   Where department_id = "18ec05" or student_id="5";
       
SELECT student_name
      ,student_id
	   FROM student
	   Where not department_id= "18ec05";
       
SELECT student_name
      ,student_id
	   FROM student
	   Where department_id like "18ec05";
       
SELECT student_name
      ,student_id
	   FROM student
	   Where department_id in ( "18ec05","18ec01");
       
SELECT student_name
      ,student_id
	   FROM student
	   Where department_id =any(select department_id from student where department_id=  "18ec05");
	
SELECT student_name
      ,student_id
      ,department_id
	   FROM student
	   Where department_id like  "18ec05";