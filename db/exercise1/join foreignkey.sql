12.Read Records by Join using tables FOREIGN KEY relationships (CROSS, INNER, LEFT, RIGHT)

SELECT student.student_id
      ,library.library_id
      ,library.library_books
      FROM student
      INNER JOIN library on student.library_id=library.library_id
	  
SELECT student.name
      ,library.library_books
      from student
      full join library on student.library_id=library.library_id;

SELECT student.name
      ,library.library_books
      from student
      left join library on student.library_id=library.library_id;

SELECT student.name
      ,library.library_books
      from student
      right join library on student.library_id=library.library_id;

SELECT * FROM student
      cross join library;	  