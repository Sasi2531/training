2.Alter Table with Add new Column and Modify\Rename\Drop column

ALTER TABLE `college`.`student` 
ADD COLUMN `age` INT NOT NULL AFTER `department_id`;

ALTER TABLE `college`.`student` 
DROP COLUMN `age`;