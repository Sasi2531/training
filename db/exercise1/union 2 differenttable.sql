13.At-least use 3 tables for Join
Union 2 Different table with same Column name (use Distinct,ALL)


ALTER TABLE `college`.`subject` 
ADD COLUMN `credit` INT NOT NULL AFTER `subject_name
ALTER TABLE `college`.`student` 
DROP PRIMARY KEY;
;

ALTER TABLE `college`.`subject` 
CHANGE COLUMN `credit` `subject_credit` INT NOT NULL ,
ADD PRIMARY KEY (`subject_id`, `subject_credit`)
;

      
ALTER TABLE `college`.`library` 
ADD COLUMN `department_id` VARCHAR(45) NOT NULL AFTER `library_books`;

UPDATE `college`.`library` SET `department_id` = '18ec08' WHERE (`library_id` = '1');
UPDATE `college`.`library` SET `department_id` = '18ec09' WHERE (`library_id` = '2');
UPDATE `college`.`library` SET `department_id` = '18ec10' WHERE (`library_id` = '3');
UPDATE `college`.`library` SET `department_id` = '18ec11' WHERE (`library_id` = '4');
UPDATE `college`.`library` SET `department_id` = '18ec12' WHERE (`library_id` = '5');

SELECT library_id
       ,department_id
       FROM student
       UNION SELECT library_id
        , department_id
        FROM library;
		
CREATE TABLE `college`.`department` (
  `department_id` VARCHAR(20) NOT NULL,
  `department_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`department_id`));

SELECT student.name
       ,library.department_id
       ,department.department_name
       ,department.department_id
       FROM student
	   INNER JOIN library on library.department_id=student.department_id
       INNER JOIN department on student.department_id=department.department_id order by library.department_id asc;