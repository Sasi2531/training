1.Create Database\table and Rename\Drop Table

CREATE SCHEMA `college` ;


CREATE TABLE `college`.`student` (
  `student_name` VARCHAR(45) NOT NULL,
  `student_id` VARCHAR(20) NOT NULL,
  `department_id` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`student_id`));

CREATE TABLE `college`.`subject` (
  `subject_id` INT NOT NULL,
  `subject_name` VARCHAR(45) NOT NULL);
  
  DROP TABLE subject;