3.Use Constraints NOT NULL, DEFAULT, CHECK, PRIMARY KEY, FOREIGN KEY, KEY, INDEX

CREATE TABLE `college`.`library` (
  `library_id` INT NOT NULL,
  `library_books` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`library_id`));

ALTER TABLE `college`.`student` 
ADD CONSTRAINT `library_id_library`
  FOREIGN KEY (`library_id`)
  REFERENCES `college`.`library` (`library_id`)
  
ALTER TABLE `college`.`student` 
ADD COLUMN `age` INT NOT NULL AFTER `department_id`;

ALTER TABLE student ADD CHECK(marks > 0);

ALTER TABLE `college`.`student` 
ADD COLUMN `student_activity` VARCHAR(45) NULL AFTER `marks`,
ADD UNIQUE INDEX `student_activity_UNIQUE` (`student_activity` ASC) VISIBLE;
;
CREATE INDEX idx_marks
   ON student (marks);