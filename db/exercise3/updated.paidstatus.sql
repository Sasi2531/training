7. Update PAID_STATUS and PAID_YEAR in SEMESTER_FEE table who
has done the payment. Entries should be updated based on student
ROLL_NUMBER.
a) Single Update - Update one student entry
b) Bulk Update - Update multiple students entries
c) PAID_STATUS value as ‘Paid’
d) PAID_YEAR value as ‘year format’

UPDATE semester_fee  
SET paid_year = (2020)
WHERE stud_id = 5;
UPDATE semester_fee  
SET paid_status = 'notpaided'
WHERE stud_id = 9; 
UPDATE semester_fee
SET paid_status =
  (case stud_id when 2 then 'paided'
                when 7 then 'paided'
    end)
WHERE stud_id in(2,7);
UPDATE semester_fee
SET paid_year =
  (case stud_id when 1 then (2021)
                when 5 then (2020)
                
    end)
WHERE stud_id in(1,5); 
SELECT * FROM semester_fee;