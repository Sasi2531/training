6. Create new entries in SEMESTER_FEE table for each student from all
the colleges and across all the universities. These entries should be
created whenever new semester starts.. Each entry should have below
default values;
a) AMOUNT - Semester fees
b) PAID_YEAR - Null
c) PAID_STATUS - Unpaid

ALTER TABLE `erdiagram`.`semester_fee` 
DROP FOREIGN KEY `stud_id`;
ALTER TABLE `erdiagram`.`semester_fee` 
CHANGE COLUMN `stud_id` `stud_id` INT NULL ,
CHANGE COLUMN `semester` `semester` TINYINT NULL ,
CHANGE COLUMN `paid_status` `paid_status` VARCHAR(10) NULL DEFAULT 'notpaided' ;
ALTER TABLE `erdiagram`.`semester_fee` 
ADD CONSTRAINT `stud_id`
  FOREIGN KEY (`stud_id`)
  REFERENCES `erdiagram`.`student` (`id`);

INSERT INTO `erdiagram`.`semester_fee` (`cdept_id`, `stud_id`, `semester`, `amount`, `paid_year`) VALUES ('1', '10', '1', '75000.00', 2016);
INSERT INTO `erdiagram`.`semester_fee` (`cdept_id`, `stud_id`, `semester`, `paid_year`) VALUES ('2', '11', '2', 2015);
INSERT INTO `erdiagram`.`semester_fee` (`cdept_id`, `stud_id`, `semester`, `paid_year`) VALUES ('3', '12', '1', 2014);
