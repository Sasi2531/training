1. Select College details which are having IT / CSC departments across allthe universities. Result should have below details;(Assume that one of
the Designation name is ‘HOD’ in Designation table)
CODE, COLLEGE_NAME, UNIVERSITY_NAME, CITY, STATE,
YEAR_OPENED, DEPTARTMENT_NAME, HOD_NAME


SELECT college.code
      ,college.name as college_name
	  ,university.university_name
	  ,college.city
	  ,college.statel
	  ,college.year_opened
	  ,department.dept_name as dept_name
	  ,employee.name as hod_name 
FROM college
	  ,university
	  ,department
	  ,employee
	  ,designation
	  ,college_department 
WHERE university.univ_code=college.univ_code
  AND employee.college_id=college.id
  AND college_department.cdept_id=employee.cdept_id
  AND designation.id=employee.desig_id
  AND department.dept_code=college_department.udept_code
  AND designation.name="hod"
HAVING department.dept_name="cse";