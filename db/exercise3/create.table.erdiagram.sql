CREATE SCHEMA `erdiagram` ;

CREATE TABLE `erdiagram`.`university` (
  `univ_code` CHAR(4) NOT NULL,
  `university_name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`univ_code`));

CREATE TABLE `erdiagram`.`college` (
  `id` INT NOT NULL,
  `code` CHAR(4) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `univ_code` CHAR(4) NOT NULL,
  `city` VARCHAR(50) NOT NULL,
  `statel` VARCHAR(50) NOT NULL,
  `year_opened` YEAR(4) NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `erdiagram`.`college_department` (
  `cdept_id` INT NOT NULL,
  `udept_code` CHAR(4) NOT NULL,
  `college_id` INT NOT NULL,
  PRIMARY KEY (`cdept_id`));

CREATE TABLE `erdiagram`.`department` (
  `dept_code` CHAR(4) NOT NULL,
  `dept_name` VARCHAR(50) NOT NULL,
  `univ_code` CHAR(4) NOT NULL,
  PRIMARY KEY (`dept_code`));
  
 CREATE TABLE `erdiagram`.`designation` (
  `id` INT NOT NULL,
  `name` VARCHAR(30) NOT NULL,
  `rank` CHAR(1) NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `erdiagram`.`employee` (
  `id` INT NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `dob` DATE NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `phone` BIGINT NOT NULL,
  `college_id` INT NOT NULL,
  `cdept_id` INT NOT NULL,
  `desig_id` INT NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `erdiagram`.`professor_syllabus` (
  `emp_id` INT NOT NULL,
  `syllabus_id` INT NOT NULL,
  `semester` TINYINT(5) NOT NULL);

CREATE TABLE `erdiagram`.`semester_fee` (
  `cdept_id` INT NOT NULL,
  `stud_id` INT NOT NULL,
  `semester` TINYINT(5) NOT NULL,
  `amount` DOUBLE(18,2) NULL,
  `paid_year` YEAR(4) NULL,
  `paid_status` VARCHAR(10) NOT NULL);
  
  CREATE TABLE `erdiagram`.`semester_result` (
  `stud_id` INT NOT NULL,
  `syllabus_id` INT NOT NULL,
  `semester` TINYINT(5) NOT NULL,
  `grade` VARCHAR(2) NOT NULL,
  `credits` FLOAT NOT NULL,
  `result_date` DATE NOT NULL);

CREATE TABLE `erdiagram`.`syllabus` (
  `id` INT NOT NULL,
  `cdept` INT NOT NULL,
  `syllabus_code` CHAR(4) NOT NULL,
  `syllabus_name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`));