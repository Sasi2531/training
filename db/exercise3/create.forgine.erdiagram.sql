ALTER TABLE `erdiagram`.`college` 
ADD INDEX `univ_code_university_idx` (`univ_code` ASC) VISIBLE;
;
ALTER TABLE `erdiagram`.`college` 
ADD CONSTRAINT `univ_code`
  FOREIGN KEY (`univ_code`)
  REFERENCES `erdiagram`.`university` (`univ_code`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `erdiagram`.`department` 
ADD INDEX `univ_code_college_idx` (`univ_code` ASC) VISIBLE;
;
ALTER TABLE `erdiagram`.`department` 
ADD CONSTRAINT `univ_code_college`
  FOREIGN KEY (`univ_code`)
  REFERENCES `erdiagram`.`university` (`univ_code`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
  
  CREATE TABLE `erdiagram`.`student` (
  `id` INT NOT NULL,
  `roll_number` CHAR(8) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `dob` DATE NOT NULL,
  `gender` CHAR(1) NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `phone` BIGINT(4) NOT NULL,
  `address` VARCHAR(200) NOT NULL,
  `academic_year` YEAR(4) NOT NULL,
  `cdept_id` INT NOT NULL,
  `college_id` INT NOT NULL,
  PRIMARY KEY (`id`));
  
ALTER TABLE `erdiagram`.`college_department` 
ADD INDEX `college_id_idx1` (`college_id` ASC) VISIBLE;
;
ALTER TABLE `erdiagram`.`college_department` 
ADD CONSTRAINT `college_id`
  FOREIGN KEY (`college_id`)
  REFERENCES `erdiagram`.`college` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

  ALTER TABLE `erdiagram`.`college_department` 
CHANGE COLUMN `college_id` `college_id` INT NULL ;

ALTER TABLE `erdiagram`.`college_department` 
ADD INDEX `udept_code_idx` (`udept_code` ASC) VISIBLE;
;
ALTER TABLE `erdiagram`.`college_department` 
ADD CONSTRAINT `udept_code`
  FOREIGN KEY (`udept_code`)
  REFERENCES `erdiagram`.`department` (`dept_code`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `erdiagram`.`employee` 
CHANGE COLUMN `college_id` `college_id` INT NULL ;

ALTER TABLE `erdiagram`.`employee` 
ADD CONSTRAINT `college_id_employee`
  FOREIGN KEY (`college_id`)
  REFERENCES `erdiagram`.`college` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `erdiagram`.`employee` 
ADD INDEX `cdept_id_idx` (`cdept_id` ASC) VISIBLE;
;
ALTER TABLE `erdiagram`.`employee` 
ADD CONSTRAINT `cdept_id`
  FOREIGN KEY (`cdept_id`)
  REFERENCES `erdiagram`.`college_department` (`cdept_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  
ALTER TABLE `erdiagram`.`employee` 
ADD INDEX `desig_id_idx` (`desig_id` ASC) VISIBLE;
;
ALTER TABLE `erdiagram`.`employee` 
ADD CONSTRAINT `desig_id`
  FOREIGN KEY (`desig_id`)
  REFERENCES `erdiagram`.`designation` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `erdiagram`.`syllabus` 
CHANGE COLUMN `cdept` `cdept` INT NULL ;

ALTER TABLE `erdiagram`.`syllabus` 
ADD CONSTRAINT `cdept_id_syllabus`
  FOREIGN KEY (`cdept`)
  REFERENCES `erdiagram`.`college_department` (`cdept_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `erdiagram`.`professor_syllabus` 
ADD INDEX `emp_id_idx` (`emp_id` ASC) VISIBLE;
;
ALTER TABLE `erdiagram`.`professor_syllabus` 
ADD CONSTRAINT `emp_id`
  FOREIGN KEY (`emp_id`)
  REFERENCES `erdiagram`.`employee` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `erdiagram`.`professor_syllabus` 
ADD INDEX `syllabus_id_idx` (`syllabus_id` ASC) VISIBLE;
;
ALTER TABLE `erdiagram`.`professor_syllabus` 
ADD CONSTRAINT `syllabus_id`
  FOREIGN KEY (`syllabus_id`)
  REFERENCES `erdiagram`.`employee` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `erdiagram`.`student` 
CHANGE COLUMN `cdept_id` `cdept_id` INT NULL ;

ALTER TABLE `erdiagram`.`student` 
ADD CONSTRAINT `cdept_id_student`
  FOREIGN KEY (`cdept_id`)
  REFERENCES `erdiagram`.`college_department` (`cdept_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `erdiagram`.`student` 
CHANGE COLUMN `college_id` `college_id` INT NULL ;

ALTER TABLE `erdiagram`.`student` 
ADD CONSTRAINT `college_id_student`
  FOREIGN KEY (`college_id`)
  REFERENCES `erdiagram`.`college` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `erdiagram`.`semester_fee` 
ADD INDEX `cdept_id_idx` (`cdept_id` ASC) VISIBLE;
;
ALTER TABLE `erdiagram`.`semester_fee` 
ADD CONSTRAINT `cdept_id`
  FOREIGN KEY (`cdept_id`)
  REFERENCES `erdiagram`.`college_department` (`cdept_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
ALTER TABLE `erdiagram`.`semester_fee` 
ADD INDEX `cdept_id_idx` (`cdept_id` ASC) VISIBLE;
;

ALTER TABLE `erdiagram`.`semester_fee` 
ADD CONSTRAINT `cdept_id_semester_fee`
  FOREIGN KEY (`cdept_id`)
  REFERENCES `erdiagram`.`college_department` (`cdept_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `erdiagram`.`semester_fee` 
ADD INDEX `stud_id_idx` (`stud_id` ASC) VISIBLE;
;
ALTER TABLE `erdiagram`.`semester_fee` 
ADD CONSTRAINT `stud_id`
  FOREIGN KEY (`stud_id`)
  REFERENCES `erdiagram`.`student` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `erdiagram`.`semester_result` 
CHANGE COLUMN `stud_id` `stud_id` INT NULL ;

ALTER TABLE `erdiagram`.`semester_result` 
ADD CONSTRAINT `stud_id_semester_result`
  FOREIGN KEY (`stud_id`)
  REFERENCES `erdiagram`.`student` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
ALTER TABLE `erdiagram`.`semester_result` 
CHANGE COLUMN `syllabus_id` `syllabus_id` INT NULL ;

ALTER TABLE `erdiagram`.`semester_result` 
ADD CONSTRAINT `syllabus_id_semester_result`
  FOREIGN KEY (`syllabus_id`)
  REFERENCES `erdiagram`.`syllabus` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;




