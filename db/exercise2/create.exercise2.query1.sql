CREATE SCHEMA `training_exp1` ;

CREATE TABLE `training_exp1`.`employeetable` (
  `emp_id` VARCHAR(9) NOT NULL,
  `first_name` VARCHAR(46) NOT NULL,
  `surname` VARCHAR(46) NULL,
  `dob` DATE NOT NULL,
  `date_of_joining` DATE NOT NULL,
  `annual_salary` INT NOT NULL,
  PRIMARY KEY (`emp_id`));
  
CREATE TABLE `training_exp1`.`departmenttable` (
  `department_id` VARCHAR(5) NOT NULL,
  `department_name` VARCHAR(17) NOT NULL,
  PRIMARY KEY (`department_id`));



ALTER TABLE `training_exp1`.`employeetable` 
DROP FOREIGN KEY `department_id_department`;
ALTER TABLE `training_exp1`.`employeetable` 
ADD CONSTRAINT `department_id`
  FOREIGN KEY (`department_id`)
  REFERENCES `training_exp1`.`departmenttable` (`department_id`);
