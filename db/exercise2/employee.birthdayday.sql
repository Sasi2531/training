SELECT dob
      ,sysdate()
FROM   employeetable 
Where  date_format(dob,'%d-%m') = date_format(sysdate(),'%d-%m');