# Full Stack Dev - Training

> This repository is created to host the files and information for the **Assessment-1** by our Group.

## Assessment 1 - Class Diagrams

### Domain - Education
#### File Stack
- Rough Notes - [Mind Map](https://bitbucket.org/Sasi2531/project1_repo/src/master/notes-mindmap/notes%20mindmap.pdf)
- Class Diagram


### App    - Amazon
- Rough Notes 
- Class Diagram

## Authors
- Arun Pandian R
- Arun Prasanth 
- Menaga
- Sasi Rekha